; For Integrator
; J.Abian 30 july 2010 - march 2015

#define MyAppName ReadIni(SourcePath + "install.ini", "Common", "name", "noname")
#define MyVersion ReadIni(SourcePath + "install.ini", "Common", "version", "0.0.0")
#define MyImageDir ReadIni(SourcePath + "install.ini", "Common", "imgdir", "")
#define MyWizardDir ReadIni(SourcePath + "install.ini","Common", "imgdir", "")
#define MyBigIcon ReadIni(SourcePath + "install.ini", "Common", "big_icon", "")
#define MySmallIcon ReadIni(SourcePath + "install.ini", "Inno","small_icon", "")
#define MyWizardImage ReadIni(SourcePath + "install.ini","Inno","wzimg","")
#define MyWizardSecondImage ReadIni(SourcePath + "install.ini","Inno","wzsimg","")
#define MyPswd ReadIni(SourcePath + "install.ini", "Inno", "pswd", "lpcsicuab")
#define MyInstall ReadIni(SourcePath + "install.ini", "Inno", "install", "INSTALL.txt")
#define MyReadMe ReadIni(SourcePath + "install.ini", "Inno", "readme", "README.txt")
#define MyLicence ReadIni(SourcePath + "install.ini", "Inno", "licence", "LICENCE.txt")
#define MyComment "Integrator. A search result integration program for proteomics."

[Setup]
AppName={#MyAppName}
;data for unins000.dat file
AppId={#MyAppName} {#MyVersion}
;appears in the first page of the installer
;AppVerName={cm:NameAndVersion,{#MyAppName},{cm:Myvers}}
;appears in the support info for add/remove programs
AppVersion={#MyVersion}
AppPublisher=LP-CSIC/UAB
DefaultDirName={pf}\{#MyAppName}_{#MyVersion}
UsePreviousAppDir=no
DefaultGroupName=LP-CSIC-UAB
Compression=lzma/max
AllowNoIcons=yes
AllowRootDirectory=yes
UsePreviousLanguage=no
UninstallDisplayIcon={#MyImageDir}\{#MySmallIcon}
OutputBaseFilename={#MyAppName}_{#MyVersion}_setup
OutputDir=installer
InfoAfterFile={#MyInstall}
LicenseFile={#MyLicence}
Password={#MyPswd}
WizardImageFile={#MyImageDir}\{#MyWizardImage}
#ifdef MyWizardSecondImage
  WizardSmallImageFile={#MyWizardDir}\{#MyWizardSecondImage}
#endif
AppCopyright=Pending 2015 Joaquin Abian
;appears in properties "version del archivo" and "version del producto"
;of the Setup.exe program in the "Version" page
;and also in the info when Setup.exe is selected with the cursor and where it adds a zero
VersionInfoVersion={#MyVersion}
SetupIconFile={#MyImageDir}\{#MyBigIcon}

[Files]
Source: "dist\*"; DestDir: "{app}"
Source: {#MyImageDir}\{#MySmallIcon}; DestDir:{app}
Source: {#MyImageDir}\{#MyBigIcon}; DestDir:{app}
Source: {#MyReadMe}; DestDir:{app}
Source: {#MyLicence}; DestDir:{app}
Source: {#MyInstall}; DestDir:{app}

#if FileExists(SourcePath + "doc\README.html")
  Source: "doc\*"; DestDir: "{app}\doc"
#endif

[Tasks]
;CreateDesktopIcon is defined in Default.isl
Name: desktopicon; Description: "{cm:CreateDesktopIcon}"

[Icons]
Name: "{group}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe" ; IconFilename:{app}\{#MySmallIcon};WorkingDir: "{app}"; Comment: {#MyComment}
#if FileExists(SourcePath + "doc\README.html")
  Name: "{group}\{#MyAppName} Documentation"; Filename: "{app}\doc\README.html" ;WorkingDir: "{app}\doc"; Comment: "{#MyAppName} Documentation"
#endif
Name: "{group}\{#MyAppName} Web Page"; Filename: "https://code.google.com/p/lp-csic-uab/wiki/Integrator"; Comment: "https://code.google.com/p/lp-csic-uab/wiki/Integrator"
Name: "{group}\LP-CSIC-UAB Web Page"; Filename: "http://proteomica.uab.cat"; Comment: "http://proteomica.uab.cat"
Name: "{userdesktop}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe"; IconFilename:{app}\{#MyBigIcon}; WorkingDir: "{app}"; Comment: {#MyComment}; Tasks: desktopicon
