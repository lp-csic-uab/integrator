#!/usr/bin/perl -w
use strict;
use warnings;
use TAP::Harness;
use FindBin qw($Bin);

push @INC, $Bin;

my @test_files = (
                  #"t/test_ascore.t",
                  #"t/test_calc_mz.t",
                  #"t/test_collection.t",
                  #"t/test_convert",
                  #"t/test_collection_2.t",
                  #"t/test_convert_discoverer.t",
                  #"t/test_convert_peaks.t",
                  #"t/test_convert_easy.t",
                  #"t/test_convert_phenyx.t",
                  #"t/test_parsing_peaks.t",
                  "t/test_parsing_discoverer.t",
                  #"t/test_search_fasta_db.t",
                 );

my %args = (
            verbosity => 1,
            merge => 0,
            );

my $harness = TAP::Harness->new( \%args );
$harness->runtests(@test_files);


1;                  #25
