
---

**WARNING!**: This is the *Old* source-code repository for Integrator program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/integrator/) located at https://sourceforge.net/p/lp-csic-uab/integrator/**  

---  
  
![https://lh6.googleusercontent.com/-VZFIf3jBTdM/UfKLzGRHx4I/AAAAAAAAAV8/1RCOae_Mubw/s800/integrator.png](https://lh6.googleusercontent.com/-VZFIf3jBTdM/UfKLzGRHx4I/AAAAAAAAAV8/1RCOae_Mubw/s800/integrator.png)


---

**WARNING!**: This is the *Old* source-code repository for Integrator program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/integrator/) located at https://sourceforge.net/p/lp-csic-uab/integrator/**  

---  
  

**Table Of Contents:**

[TOC]

#### Description

**Integrator** is a easy to use GUI based, [Perl](http://www.perl.org/) application that integrates proteomic search results obtained from three different proteomic search engines; OMSSA, Sequest and Phenyx; for the same [mass spectrometry](http://en.wikipedia.org/wiki/Mass_spectrometry) scan file.

![http://lh3.ggpht.com/_ty9pOjnnHDA/TOP8x83Aq6I/AAAAAAAAAFI/7sVqtJQkoLc/Integrator1.png](http://lh3.ggpht.com/_ty9pOjnnHDA/TOP8x83Aq6I/AAAAAAAAAFI/7sVqtJQkoLc/Integrator1.png)

This allows to easily get consensus information and a confident identification with no extra filtering of search results.
Moreover, the output of the integration process is saved in the standard [JSON format](http://en.wikipedia.org/wiki/JSON).

The **Integrator** also allows users to effortless generate reports.

![http://lh3.ggpht.com/_ty9pOjnnHDA/TOP8xnrBQbI/AAAAAAAAAFE/81qmfI_HxqU/Integrator2.png](http://lh3.ggpht.com/_ty9pOjnnHDA/TOP8xnrBQbI/AAAAAAAAAFE/81qmfI_HxqU/Integrator2.png)

#### Installation

Tested in Windows XP 32 bits and Windows 7 64 bits.

##### External Requirements

  * None if you use the [Windows Installer](https://bitbucket.org/lp-csic-uab/integrator/downloads) (see below).

##### From Windows Installer

  1. Download `Integrator_x.y.z_setup.exe` [Windows Installer](https://bitbucket.org/lp-csic-uab/integrator/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `Integrator` by double-clicking on the `Integrator` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install [Perl](http://www.perl.org/) and other third party software indicated in [Dependencies](#markdown-header-source-dependencies), as required.
  1. Download `Integrator` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/integrator/src).
  1. Copy the `Integrator` folder in your path. If you only plan to run the program from a particular user, you will need only to put `Integrator` folder inside a user folder.
  1. Run `Integrator` by double-clicking on the `integrator.pl` module, or typing  `perl integrator.pl`  on the command line.

###### _Source Dependencies:_

  * [Perl](http://www.perl.org/) 5.16 (not tested with other versions)
  * [wxPerl](http://www.wxperl.it/) 0.9921 (with Alien::wxWidgets 0.64)

Third-party software and package versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

#### Download

You can download the Windows Installer for the last version of **`Integrator`** [here](https://bitbucket.org/lp-csic-uab/integrator/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/integrator/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code.


---

**WARNING!**: This is the *Old* source-code repository for Integrator program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/integrator/) located at https://sourceforge.net/p/lp-csic-uab/integrator/**  

---  
  
