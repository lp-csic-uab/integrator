package Core::Integration;
use utf8;
use strict;
use warnings;
use Data::Dumper;

our $VERSION = '1.00';
our @EXPORT_OK = qw(integrate extract_spectra calculate_mz search_fasta_db);
use base qw(Exporter);

BEGIN {push @INC, '../'}

use JSON::XS;
use Core::Ascore qw(ascore);
use Core::IntConsts qw(get_masas_aa get_masas_mod);
use Parsers::Sequest;
use Parsers::Omssa;
use Parsers::Phenyx;
use Parsers::EasyProt;
use Parsers::Peaks;
use Parsers::Discoverer;
use Parsers::None;
use Data::Dumper;

my %masas_aa = %{&get_masas_aa};
my %masas_mod = %{&get_masas_mod};

sub integrate{
	my @fst_files = @{$_[0]};
	my @snd_files = @{$_[1]};
	my @trd_files = @{$_[2]};
	my $fst_engine = $_[3];
	my $snd_engine = $_[4];
	my $trd_engine = $_[5];
	my $database = $_[6];
	my $mgf_list = $_[7];
	my $report_file = $_[8];
	my $one_engine = $_[9];
	my $scan_method = $_[10];
	my $is_tmt_or_itraq = $_[11];
	my $offset = $_[12];
	my $precision = $_[13];
	my $experiment = $_[14];

	my %parsers = (Sequest    => \&Parsers::Sequest::analyze,
                   Omssa      => \&Parsers::Omssa::analyze,
                   Phenyx     => \&Parsers::Phenyx::analyze,
				   EasyProt   => \&Parsers::EasyProt::analyze,
	               Peaks      => \&Parsers::Peaks::analyze,
				   Discoverer => \&Parsers::Discoverer::analyze,
				   None       => \&Parsers::None::analyze
	);

    # parse engine result files
	# $mgf_list is sent only because Peaks need them to get second scan
	my %fst_info = $parsers{$fst_engine}->(\@fst_files, $mgf_list);
	my %snd_info = $parsers{$snd_engine}->(\@snd_files, $mgf_list);
	my %trd_info = $parsers{$trd_engine}->(\@trd_files, $mgf_list);

	# get params from each engine
	my $fst_params = delete $fst_info{'params'};
	my $snd_params = delete $snd_info{'params'};
	my $trd_params = delete $trd_info{'params'};

	my %data;
	my @spectra_list;
	my @consensus_peptides;
	my %spectra;              # saves spectrum names in keys. Value is not used.

	foreach (keys %fst_info){$spectra{$_} = 1};
	foreach (keys %snd_info){$spectra{$_} = 1};
	foreach (keys %trd_info){$spectra{$_} = 1};

	if ($fst_engine ne 'None') {
		print 'number of ' . $fst_engine . ' spectra:  ' . keys(%fst_info) . ".\n";
	}
	if ($snd_engine ne 'None') {
		print 'number of ' . $snd_engine . ' spectra:  ' . keys(%snd_info) . ".\n";
	}
	if ($trd_engine ne 'None') {
		print 'number of ' . $trd_engine . ' spectra:  ' . keys(%trd_info) . ".\n";
	}
	print 'number of TOTAL spectra:  ' . keys(%spectra) . ".\n";

	print "Joining search engines\n";

	foreach my $spectrum(keys %spectra){
		# print $spectrum, "\n";
		# Joining the info from different search engines and
		# creating a consensus peptide
		# TODO: 'ms' key is rewritten when more than one engine match.
		#       - Not neccesarily the same value
		#       - peaks do not gives ms level so it would fail
		if(defined $fst_info{$spectrum}){
			$data{$spectrum}{$fst_engine} = $fst_info{$spectrum};
			$data{$spectrum}{'ms'} = $fst_info{$spectrum}{'ms'};
		}
		if(defined $snd_info{$spectrum}){
			$data{$spectrum}{$snd_engine} = $snd_info{$spectrum};
			$data{$spectrum}{'ms'} = $snd_info{$spectrum}{'ms'};
		}
		if(defined $trd_info{$spectrum}){
			$data{$spectrum}{$trd_engine} = $trd_info{$spectrum};
			$data{$spectrum}{'ms'} = $trd_info{$spectrum}{'ms'};
		}

		my ($consensus, $consensus_mod, $consensus_z)
		    = consensus($data{$spectrum},
				        $fst_engine, $snd_engine, $trd_engine
					    );

		# If only one engine,
		# do not remove any key and copy consensus from that engine
		if ($one_engine){
			$data{$spectrum}{'consensus'}
				= $data{$spectrum}{$one_engine}{'unmodified_sequence'};
			$data{$spectrum}{'consensus_mod'}
				= $data{$spectrum}{$one_engine}{'modified_sequence'};
			$data{$spectrum}{'consensus_z'}
				= $data{$spectrum}{$one_engine}{'z'};

			push @consensus_peptides, $data{$spectrum}{$one_engine}{'unmodified_sequence'};
			push @spectra_list, $spectrum;
		}
		elsif ($consensus){
			$data{$spectrum}{'consensus'} = $consensus;
			$data{$spectrum}{'consensus_mod'} = $consensus_mod;
			$data{$spectrum}{'consensus_z'} = $consensus_z;
			push @consensus_peptides, $consensus;
			push @spectra_list, $spectrum;
		}
		# Deleting the non-consensus data if more than one search engine
		else {
			delete $data{$spectrum};
			delete $spectra{$spectrum};
			next;
		}

		# Deleting spectra with double modification <- WHY???
		if($data{$spectrum}{'consensus_mod'} =~ /\,/){
			delete $data{$spectrum};
			delete $spectra{$spectrum};
			print $spectrum, "\t->\tDeleted because of double mod at N-terminus\n";
			next;
		}

		# Searching for the MS3->MS2 info
		if($data{$spectrum}{'ms'} == 3){
			my $file = get_ms2_file_from_ms3($spectrum, $scan_method);
			push @spectra_list, $file;
		}
		#Adding Experimental Conditions reference
		$data{$spectrum}{'experiment'} = $experiment;
	}

	print "number of spectra:  " . keys(%data) . ".\n";

	#Obtain the complete list of proteins containing the peptide
	print "Obtaining protein list\n";
	my $db_matches = search_fasta_db($database, @consensus_peptides);
	my %db_matches = %{$db_matches};
	foreach my $spectrum(keys %spectra){
		my $consensus = $data{$spectrum}{'consensus'};
		if (defined $db_matches{$consensus}{'seq'}) {
			$data{$spectrum}{'consensus'} = $db_matches{$consensus}{'seq'};
		}
		$data{$spectrum}{'consensus_proteins'}
			= $db_matches{$consensus}{'prots'};
	    $data{$spectrum}{'consensus_first_prot'}
			= $db_matches{$consensus}{'first_prot'};
		$data{$spectrum}{'consensus_first_prot_name'}
			= $db_matches{$consensus}{'first_prot_name'};

	}

	# Obtain the spectra info
	# (mass and intensity arrays and peptide experimental mass)
	print "Obtaining spectra\n";
	my %spectral_data = extract_spectra(\@spectra_list, $mgf_list);
	foreach my $spectrum(keys %data){
		if(defined $spectral_data{$spectrum}{'parent'}){
			$data{$spectrum}{'spectral_info'}{'intensity_array'}
				= $spectral_data{$spectrum}{'intensities'};
			$data{$spectrum}{'spectral_info'}{'mass_array'}
				= $spectral_data{$spectrum}{'masses'};
			$data{$spectrum}{'spectral_info'}{'parent_mass'}
				= $spectral_data{$spectrum}{'parent'};
		}
		else{
			print "Mgf info not found for the file $spectrum \n";
		}
		if ($data{$spectrum}{'ms'} == 3){
			my $file = get_ms2_file_from_ms3($spectrum, $scan_method);
			if(defined $spectral_data{$file}{'parent'}){
				$data{$spectrum}{'ms2_spectral_info'}{'intensity_array'}
					= $spectral_data{$file}{'intensities'};
				$data{$spectrum}{'ms2_spectral_info'}{'mass_array'}
					= $spectral_data{$file}{'masses'};
				$data{$spectrum}{'ms2_spectral_info'}{'parent_mass'}
					= $spectral_data{$file}{'parent'};
				$data{$spectrum}{'ms2_spectral_info'}{'ms2_file'}
					= $file;
			}
			else{
				print "MS2 info not found for the file $file \n";
			}
		}
	}

	#Obtaining the consensus deltamz
    print "Obtaining consensus deltamz\n";
	foreach my $spectrum(keys %data){
		my $mz = calculate_mz($data{$spectrum}{'consensus_mod'},
							  $data{$spectrum}{'consensus_z'});

		$data{$spectrum}{'consensus_deltamz'}
			= $mz - $data{$spectrum}{'spectral_info'}{'parent_mass'};
	}

	#Obtaining quantitative iTRAQ/TMT info
	my $myfunc;
	my $infokey;
	if($is_tmt_or_itraq eq "itraq"){
		$myfunc = \&analyze_itraq;
		$infokey = 'itraq_info';
	}
	elsif($is_tmt_or_itraq eq "tmt"){
		$myfunc = \&analyze_tmt;
		$infokey = 'tmt_info';
	}
	unless ($is_tmt_or_itraq eq "none"){
        print "Obtaining quantitative iTRAQ/TMT info\n";
		my $info = '';
		my $low_offset = $offset - $precision;
		my $high_offset = $offset + $precision;
		foreach my $spectrum(keys %data){
			if ($data{$spectrum}{'ms'} == 2){
				if(defined $data{$spectrum}{'spectral_info'}{'intensity_array'}){
					$info = &{$myfunc}($data{$spectrum}{'spectral_info'}{'mass_array'},
									   $data{$spectrum}{'spectral_info'}{'intensity_array'},
									   $low_offset, $high_offset);
				}
			}
			elsif ($data{$spectrum}{'ms'} == 3){
				if(defined $data{$spectrum}{'ms2_spectral_info'}{'intensity_array'}){
					$info = &{$myfunc}($data{$spectrum}{'ms2_spectral_info'}{'mass_array'},
									   $data{$spectrum}{'ms2_spectral_info'}{'intensity_array'},
									   $low_offset, $high_offset);
				}
			}
			$data{$spectrum}{$infokey} = $info;
		}
	}

	# Obtaining Ascore info
	print "Calculating Ascore for phosphopeptides\n";
	my $counter_outer = 0;
	my $counter_inner = 0;
	$| = 1;    # set autoflushing on for printing dots
	foreach my $spectrum(keys %data){
		if ((defined $data{$spectrum}{'consensus_mod'})and
			(defined $data{$spectrum}{'spectral_info'}{'parent_mass'})
		   ){
			if($data{$spectrum}{'consensus_mod'} =~ /(\(21\)|\(23\))/){
				$counter_outer += 1;
				if ($counter_outer == 10) {
					$counter_inner += 1;
					print '.';
					$counter_outer = 0;
					if ($counter_inner == 100) {
						print "\n";
					}
				}

				my %ascore_data
					= ascore($data{$spectrum}{'consensus_mod'},
							 $data{$spectrum}{'spectral_info'}{'parent_mass'},
							 $data{$spectrum}{'consensus_z'},
							 $data{$spectrum}{'spectral_info'}{'mass_array'},
							 $data{$spectrum}{'spectral_info'}{'intensity_array'},
							 0.5, 2000, 200);
				$data{$spectrum}{'ascore_data'} = \%ascore_data;
			}
		}
	}
	print "\n";
	$| = 0;

	### Metadata for JSON database ###
	# include engines used
	$data{'fst_engine'} = $fst_engine;
	$data{'snd_engine'} = $snd_engine;
	$data{'trd_engine'} = $trd_engine;
	#include params for extra columns in reports:
	$data{'fst_params'} = $fst_params;
	$data{'snd_params'} = $snd_params;
	$data{'trd_params'} = $trd_params;
	###################################

	my $json = JSON::XS->new;
	my $json_text = $json->pretty->encode(\%data);
	my $error = open my $SAL, ">", "$report_file";
	print $SAL $json_text;
	close $SAL;
	print "======================\n";
	print " Integration complete\n";
	print "======================\n";
	return 1;
}


sub get_ms2_file_from_ms3{
	my ($spectrum, $scan_method) = @_;
	my ($file, $scan1, $scan2) = split /\./, $spectrum;
	if ($scan_method == 0){                   #cid
		$scan1 = $scan1 - 1;
		$scan2 = $scan1;
	}
	elsif ($scan_method == 1){                #hcd
		$scan1 = $scan1 - 2;
		$scan2 = $scan1 + 1;
	}
	else{
		$scan1 = $scan1 - 4;                   #pqd
		$scan2 = $scan1 + 3;
	}
	return "$file.$scan1.$scan2";
}


sub consensus{
	my %data_spectrum = %{$_[0]};
	my $fst_engine = $_[1];
	my $snd_engine = $_[2];
	my $trd_engine = $_[3];

	# trick for the following comparison: give different default values.
	my ($pept_fst, $pept_snd, $pept_trd) = ('0', '1', '2');
	my ($all_i_fst, $all_i_snd, $all_i_trd) = ('0', '1', '2');
	my $pept_cons;
	my $pept_cons_mod;
	my $z_cons;

	if(defined $data_spectrum{$fst_engine}{'unmodified_sequence'}){
		$pept_fst = $data_spectrum{$fst_engine}{'unmodified_sequence'};
		($all_i_fst = $pept_fst) =~ s/L/I/g;
	}
	if(defined $data_spectrum{$snd_engine}{'unmodified_sequence'}){
		$pept_snd = $data_spectrum{$snd_engine}{'unmodified_sequence'};
		($all_i_snd = $pept_snd) =~ s/L/I/g;
	}
	if(defined $data_spectrum{$trd_engine}{'unmodified_sequence'}){
		$pept_trd = $data_spectrum{$trd_engine}{'unmodified_sequence'};
		($all_i_trd = $pept_trd) =~ s/L/I/g;
	}

	# can not be done so easily !? -> ascore!, peak assignation!
	if ($all_i_fst eq $all_i_snd){
		$pept_cons = $pept_fst;
		$pept_cons_mod = $data_spectrum{$fst_engine}{'modified_sequence'};
		$z_cons = $data_spectrum{$fst_engine}{'z'};
	}
	elsif ($all_i_fst eq $all_i_trd){
		$pept_cons = $pept_fst;
		$pept_cons_mod = $data_spectrum{$fst_engine}{'modified_sequence'};
		$z_cons = $data_spectrum{$fst_engine}{'z'};
	}
	elsif ($all_i_snd eq $all_i_trd){
		$pept_cons = $pept_snd;
		$pept_cons_mod = $data_spectrum{$trd_engine}{'modified_sequence'};
		$z_cons = $data_spectrum{$trd_engine}{'z'};
	}

	return ($pept_cons, $pept_cons_mod, $z_cons);
}

sub search_fasta_db{
	my $fasta = shift;
	my @pepts = @_;
	my %matches;
	my %seq_match;
	my %patterns;
	my $t0;

	$t0 = time();

	# remove duplicated sequences for searching
	my %nr_pepts;
	undef @nr_pepts{@pepts};
	my @nr_pepts = keys %nr_pepts;

	# calculate patterns for database search with I and L
	foreach (@nr_pepts){
		(my $pat = $_) =~ s/[IL]/[I|L]/g;
		$patterns{$_} = qr/$pat/;
	}

	$/ = "\n>";
	my $error = open my $FASTA, '<', $fasta;
	#Set cursor to start of file + 1 char (skip first ">")
	seek($FASTA, 1, 0);

	# search fasta
	my $first_counter = 0;
	my $second_counter = 0;
	$| = 1;    # set autoflushing on for printing dots
	while (<$FASTA>) {
		my ($title, $seq) = split(/\n/o, $_, 2);
		$seq =~ s/\n//g;

		$first_counter += 1;
		if ($first_counter == 100) {
			$first_counter = 0;
			print '.';
			$second_counter += 1;
			if ($second_counter == 100) {
				$second_counter = 0;
				print "\n";
			}
		}
		foreach (@nr_pepts){
			if ($seq =~ m/($patterns{$_})/){
				# assumes matching againts decoy database ... ?
				if (index($title, 'decoy') != -1) {
					# give decoys a title mimicking a regular entry title
					$title = "$title|$title|$title";
				}
				if (defined $matches{$_}){
					$matches{$_}{'prots'} =
					    $matches{$_}{'prots'} . "||" . $title;
					push @{$seq_match{$_}}, $1;
				}
				else {
					$matches{$_}{'prots'} = $title;
					$seq_match{$_} = [$1];
				}
			}
		}
	}
	print "\n";
	$| = 0;    # set autoflushing off
	close $FASTA;

	# clean protein data (short by name and get only first description)
	foreach my $pept (@nr_pepts){
		my @titles;
		if (defined $matches{$pept}{'prots'}) {
			@titles = split /\|\|/, $matches{$pept}{'prots'};
		}
		else{
			@titles = "decoy|decoy|decoy";
			print "Unmatched entry $pept with current database\n";
		}
		my @atomic_titles = map {[split /\|/, $_]} @titles;
		my %title_dict = map {@$_[1], @$_[2]} @atomic_titles;
		my @accession = sort keys %title_dict;
		my $first_accession = $accession[0];

		my $first_title = $title_dict{$first_accession};
		$first_title = (split / OS=/, $first_title)[0];

		$matches{$pept}{'prots'}      = join '|', @accession;
		$matches{$pept}{'first_prot'} = $first_accession;
		$matches{$pept}{'first_prot_name'} = $first_title;
	}

	# produce sequence with J
	# if both I and L alternate sequences are found in the db.
	while (my ($pept, $alt) = each %seq_match){
		if (@$alt > 1){
			my %unique;
			undef @unique{@$alt};
			my @unique_alt = keys %unique;
			if (@unique_alt > 1){
				my $first = shift @unique_alt;
				my @first = split(//, $first);
				for my $other (@unique_alt){
					my @other = split(//, $other);
					foreach (0 .. $#first){
						if ($other[$_] ne $first[$_]){
							$first[$_] = "J";
						}
					}
				}
				$matches{$pept}{'seq'} = join '', @first;
			}
			else{
				$matches{$pept}{'seq'} = shift @unique_alt;
			}
		}
		else{
			$matches{$pept}{'seq'} = shift @$alt;
		}
	}
	print 'time for database search:', time() - $t0, " secs\n";
	#init = 66 sec
	#after qr and $_ in 'for my $pept (@nr_pepts)' = 68 secs!
	return {%matches};
}


sub extract_spectra{
	#  Extract the parent mass and mass/intensity values
	#	@specs	->	list of spectra (file.scan1.scan2)
	#	@mgfs	->	list of mgf file inside we'll search for spectral info
	#	return	->	%specs{$spec}{'masses'} / {'intensities'} / {'parent'}
	my @specs = @{$_[0]};
	my @mgfs = @{$_[1]};
	my %specs;

	foreach (@mgfs){
		my %spec1 = parse_mgf($_, \@specs);
		foreach(keys %spec1){
			$specs{$_} = $spec1{$_};
		}
	}
	return %specs;
}


sub parse_mgf{
	my $mgf = shift;
	my @spec_list = @{$_[0]};
	my %specs_to_return;
	foreach(@spec_list){
		$specs_to_return{$_} = 1;
	}

	open my $MGF, '<', $mgf or die $!;
	# the mgf file must finish with 1 and only 1 blank line for good split
	my @lines = do {local $/ = "END IONS\n\n"; <$MGF>};
	close $MGF;
	my %mgf_info;
	foreach my $line(@lines){
		$line =~ /BEGIN IONS\nTITLE\=(.+)\nPEPMASS\=/;
		# TITLE=29_IMAC_PI_0_15_120_20120110_Fr16,Scan:1043,MS:3,Rt:28.4827
		my $spec = (split /\,/, $1)[0];
		my $scan = (split /\,/, $1)[1];
		$scan =~ s/Scan\://;
		if($scan =~ s/\-/\./){
			$spec = "$spec.$scan";
		}
		else{
			$spec = "$spec.$scan.$scan";
		}
		if(defined $specs_to_return{$spec}){
			my @lines_n = split /\n/, $line;
			my $masses = '';
			my $intensities = '';
			my $pept_mass = $lines_n[2];
			$pept_mass =~ s/PEPMASS\=//;
			for(5..$#lines_n){
				if($lines_n[$_] =~ /END IONS/){
					last;
				};
				my ($mass, $int) = split / /, $lines_n[$_];
				$masses = $masses . '|' . sprintf("%.4f", $mass);
				$intensities = $intensities . '|' . sprintf("%.2f", $int);
			}
			$masses =~ s/\|//;
			$intensities =~ s/\|//;
			$mgf_info{$spec}{'masses'} = $masses;
			$mgf_info{$spec}{'intensities'} = $intensities;
			$mgf_info{$spec}{'parent'} = $pept_mass;
		}
	}
	return %mgf_info;;
}


sub calculate_mz{
	my ($pep, $z) = @_;
	my $mass = 18.010565;      #H2O

	while ($pep){
		if ($pep =~ s/^(\w)\((\w+),(\w+)\)//){
			$mass += $masas_aa{$1} + $masas_mod{$2} + $masas_mod{$3};
		}
		elsif ($pep =~ s/^(\w)\((\w+)\)//){
			$mass += $masas_aa{$1} + $masas_mod{$2};
		}
		elsif ($pep =~ s/^(\w)//){
			$mass += $masas_aa{$1};
		}
	}
	return ($mass + $z * 1.007825) / $z;
}


sub analyze_itraq{
	my @masses = split /\|/, $_[0];
	my @intensities = split /\|/, $_[1];
	my $min = $_[2];
	my $max = $_[3];

	my ($int_114, $int_115, $int_116, $int_117) = (0, 0, 0, 0);

	for (1..$#masses){
		if($masses[$_] > 118){
			last;
		}
		#itraq ions -> 114.1112, 115.1083, 116.1116, 117.1150
		if (($masses[$_] >= (114.1112 + $min))
			and ($masses[$_] < (114.1112 + $max))){
			$int_114 += $intensities[$_];
		}
		elsif (($masses[$_] >= (115.1083 + $min))
			   and($masses[$_] < (115.1083 + $max))){
			$int_115 += $intensities[$_];
		}
		elsif (($masses[$_] >= (116.1116 + $min))
			   and($masses[$_] < (116.1116 + $max))){
			$int_116 += $intensities[$_];
		}
		elsif (($masses[$_] >= (117.115 + $min))
			   and($masses[$_] < (117.115 + $max))){
			$int_117 += $intensities[$_];
		}
	}
	return "$int_114|$int_115|$int_116|$int_117";
}


sub analyze_tmt{
	my @masses = split /\|/, $_[0];
	my @intensities = split /\|/, $_[1];
	my $min = $_[2];
	my $max = $_[3];

	my ($int_126, $int_127, $int_128,
		$int_129, $int_130, $int_131) = (0, 0, 0, 0, 0, 0);

	for (0..$#masses){
		if($masses[$_] > 131.6){
			last;
		}
		# tmt ions -> 126.1283 127.1316 128.1350 129.1383 130.1417 131.1387
		if (($masses[$_] >= (126.1283 + $min))
			and ($masses[$_] < (126.1283 + $max))){
			$int_126 += $intensities[$_];
		}
		elsif (($masses[$_] >= (127.1316 + $min))
			   and ($masses[$_] < (127.1316 + $max))){
			$int_127 += $intensities[$_];
		}
		elsif (($masses[$_] >= (128.135 + $min))
			   and ($masses[$_] < (128.135 + $max))){
			$int_128 += $intensities[$_];
		}
		elsif (($masses[$_] >= (129.1383 + $min))
			   and ($masses[$_] < (129.1383 + $max))){
			$int_129 += $intensities[$_];
		}
		elsif (($masses[$_] >= (130.1417 + $min))
			   and ($masses[$_] < (130.1417 + $max))){
			$int_130 += $intensities[$_];
		}
		elsif (($masses[$_] >= (131.1387 + $min))
			   and ($masses[$_] < (131.1387 + $max))){
			$int_131 += $intensities[$_];
		}
	}
	return "$int_126|$int_127|$int_128|$int_129|$int_130|$int_131";
}


1;    #455
