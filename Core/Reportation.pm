package Core::Reportation;

use strict;
use warnings;

our $VERSION = '1.00';
our @EXPORT_OK = qw(read_json general_report psite_report);
use base qw(Exporter);

BEGIN {push @INC, '../'};

use JSON::XS;
use Reports::ExtReport qw(gen_ext_report);
use Reports::StatsReport qw(gen_stats_report);
use Reports::PsiteReport;


sub read_json{
	my $error = open my $IN, '<', shift;
	my $json_text = do {local $/ = (); <$IN>};
	my $json = JSON::XS->new;
	my $perl_scalar = $json->decode($json_text);
	close $IN;
	return $perl_scalar;
}


sub general_report{
	my ($json_data, $ext_report_file, $limit_ascore, $is_tmt_or_itraq) = @_;

	print "Generating extended report\n";
	$ext_report_file .= '_extended_report.xls';

	gen_ext_report($json_data, $ext_report_file,
				   $limit_ascore, $is_tmt_or_itraq
	);
	gen_stats_report($json_data, $ext_report_file, $limit_ascore);
	return 1;
}


sub psite_report{
	my ($json_data, $out_file, $ascore_limit, $is_tmt_or_itraq) = @_;

	print "Generating report_psites\n";
	my $psite_report_file = $out_file . '_psite_report';
	gen_psite_report($json_data, $psite_report_file, $ascore_limit, $is_tmt_or_itraq);
	print "\nReporting finished\n\n\n";
	return 1;
}


1;                                                                       #51
