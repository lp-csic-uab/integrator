package Core::IntConsts;
use strict;
use warnings;

our $VERSION = '1.00';
our @EXPORT = qw(get_masas_aa get_masas_mod);
use base qw(Exporter);


sub get_masas_aa{
	my %masas_aa = (G=> 57.021464, A=> 71.037114, S=> 87.032029, P=> 97.052764,
					V=> 99.068414, T=>101.04768,  C=>160.03065,
					L=>113.08406,  I=>113.08406,  N=>114.04293,  D=>115.02694,
					Q=>128.05858,  K=>128.09496,  E=>129.04259,  M=>131.04049,
					H=>137.05891,                 F=>147.06841,  R=>156.10111,
					Y=>163.06333,  W=>186.07931,  Not=>0,        J=>113.08406,
					);
					#la C esta CAM!, la considero PTM fija
	return \%masas_aa;
}

sub get_masas_mod{
	my %masas_mod = (21 =>  79.966331,     	#fosfato
					 23 => -18.010565,      #dh
					 214=> 144.102063,      #itraq
					 737=> 229.162932,      #tmt
					 35 =>  15.994915,      #oxidacion
					 Not =>  0);
	return \%masas_mod;
}

return 1;      #32

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
