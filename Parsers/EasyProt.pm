package Parsers::EasyProt;

use strict;
use warnings;

my $VERSION = '1.00';
our @EXPORT_OK = qw();
use base qw(Exporter);

use Spreadsheet::XLSX;



sub analyze{
	my ($input_files, $mgf_list) = @_;

	my @input_files = @{$input_files};
	my %full_data;

	# Parse all xls files to be joined.
	foreach(@input_files){
		print $_;
		my %parsed_input = parse($_);
		foreach(keys %parsed_input){
			$full_data{$_} = $parsed_input{$_};
		}
	}
	$full_data{'params'} = get_params();

	return %full_data;
}

sub get_params{
	my @params = ('unmodified_sequence', 'modified_sequence', 'z',
				  'mz','deltamz',
				  'zscore', 'validity',
				  'rt',
				  'db_first_match','count',
	);
	return \@params;
}

sub parse{
	my $input_file = shift;            #Excel worksheet global export
	my %data;
	my @lines;

	my $excel = Spreadsheet::XLSX->new($input_file);

	for my $sheet (@{$excel->{Worksheet}}) {
		if ($sheet->{Name} =~ /Job \d+/){
			$sheet -> {MaxRow} ||= $sheet -> {MinRow};
			for my $row ($sheet -> {MinRow} .. $sheet -> {MaxRow}) {
				$sheet -> {MaxCol} ||= $sheet -> {MinCol};
				for my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol}) {
					my $cell = $sheet->{Cells}[$row][$col];
					$lines[$row][$col] = $cell->{Val} if($cell);
				}
			}
		}
	}
	########### EasyProt File Estructure  ##############
	# Col Name              Contents
	# 0   Sequence          AAEDDEDDDVDTK
	# 1   Charge            3
	# 2   m/z               632.6298218
	# 3   zscore            5.226122
	# 4   Seq + PTMs        [TMTsixplex_Nterm]AAEDDEDDDVDTK[TMTsixplex_K]
	# 5   #PTMs             2
	# 6   RT                0.85
	# 7   Scan              Filename_Fr16,Scan:2237-2238,MS:2,Rt:51.2810
	# 8   Matched Prots     O60271-7,P06454,P06454-2
	# 9   #Matched Prots    3
	#10   Matched Main Prots  P06454-2
	#11   #Matched Main Prots 1
    ####################################################

	# if the 1st column in the 1st row does not contain 'Compound', dont parse.
	unless($lines[0][0] =~ /^Sequence/){return;}

	for (1..$#lines){
		my @cols = @{$lines[$_]};
		my $compound_info;
		if($cols[7]){$compound_info = $cols[7]} else {$compound_info = ''};
		#
		# Extract components from compound_info
		# 29_IMAC_PI_20120110_Fr16,Scan:2237-2238,MS:2,Rt:51.2810    (EasyProt)
		my ($file, $scan, $ms, $rt) = split /\,/, $compound_info;
		$scan =~ s/Scan\://;
		my ($scan1, $scan2);
		if($scan =~ /\-/){                 # if contains hyphen, split
			($scan1, $scan2) = split /\-/, $scan;
		}
		else{
			($scan1, $scan2) = ($scan, $scan);
		}
		$ms =~ s/MS\://;
		$rt =~ s/Rt\://;
		$rt = sprintf("%.0f", $rt);
		my $spectrum = $file . '.' . $scan1 . '.' . $scan2;

		my $zscore;
		if($cols[3]){$zscore = $cols[3]} else {$zscore = ''};
		# if already exists, get only (overwrite with) the higher zscore.
		if(defined $data{$spectrum}){
			if($data{$spectrum}{'zscore'} > $zscore){
				next;
			}
		}

		my $validity;  #no existe
		$validity = '';

		my $sequence;
		if ($cols[4]){$sequence = $cols[4]} else {$sequence = ''};

		my $z;
		if ($cols[1]){$z = $cols[1]} else {$z = ''};

		my $mz;
		if ($cols[2]){$mz = $cols[2]} else {$mz = ''};

		my $deltamz;  # do not exists
		$deltamz = '';

		my $dbmatch;  # can be col 8 (Matched Prots) or col 10 (matched Main Prots)
		if($cols[8]){$dbmatch = $cols[8]} else {$dbmatch = ''};

		my @prots = split ',', $dbmatch;
		my $first_prot = $prots[0];  # should this be 'main prots' ?

		$data{$spectrum}{'ms'} = $ms;
		$data{$spectrum}{'rt'} = $rt;
		$data{$spectrum}{'validity'} = $validity;

		($data{$spectrum}{'unmodified_sequence'},
		 $data{$spectrum}{'modified_sequence'}
		) = translate_sequence($sequence);

		# remove modification in C (CAM, unimod code: 4)
		$data{$spectrum}{'modified_sequence'}=~ s/\(4\)//g;
		$data{$spectrum}{'z'} = $z;
		$data{$spectrum}{'mz'} = $mz;
		$data{$spectrum}{'deltamz'} = $deltamz;
		$data{$spectrum}{'zscore'} = $zscore;
		$data{$spectrum}{'db_first_match'} = $first_prot;
		$data{$spectrum}{'count'} = 1 + $#prots;
	}
	return %data;
}

sub translate_sequence{
	my $sequence = shift;

	# $sequence --> "[TMTsixplex_Nterm]AAEDDEDDDVDTK[TMTsixplex_K]"   (EasyProt)
	my $sequence_unmod;
	($sequence_unmod = $sequence) =~ s/\[.+?\]//g;

	if($sequence =~ m/\[/){
	    $sequence =~ s/\[Cys_CAM\]/\(4\)/g;
	    $sequence =~ s/\[Oxidation_M\]/\(35\)/g;
	    $sequence =~ s/\[PHOS\]/\(21\)/g;
	    $sequence =~ s/\[deshidratacionST\]/\(23\)/g;
	    $sequence =~ s/\[Dehydrated_ST\]/\(23\)/g;
        $sequence =~ s/\[Dehydration ST\]/\(23\)/g;
	    $sequence =~ s/\[iTRAQ_K\]/\(214\)/g;
	    $sequence =~ s/\[iTRAQ_Y\]/\(214\)/g;
	    $sequence =~ s/\[iTRAQ_Nterm\]/\(214\)/g;
	    $sequence =~ s/\[TMTsixplex_K\]/\(737\)/g;
	    $sequence =~ s/\[TMTsixplex_Y\]/\(737\)/g;
	    $sequence =~ s/\[TMTsixplex_Nterm\]/\(737\)/g;
	}

	# reposition Nterm modification from left to right of the first aa
	$sequence =~ s/^\(214\)(.)/$1\(214\)/;
	$sequence =~ s/^\(737\)(.)/$1\(737\)/;

	# unify when two consecutive modifications: (x)(y)->(x,y)
	$sequence =~ s/^(\w)\((\d+)\)\((\d+)\)/$1\($2\,$3\)/;

	return ($sequence_unmod, $sequence);
}


1;   #167


__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
