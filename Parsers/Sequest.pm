package Parsers::Sequest;

use strict;
use warnings;

my $VERSION = '1.00';
our @EXPORT_OK = qw(analyze);
use base qw(Exporter);


sub analyze{
	my ($input_files, $mgf_list) = @_;
	
	my @input_files = @{$input_files};
	my %full_data;

	# Parse all xls files to be joined.
	foreach(@input_files){
		if (-B $_) {
			print "File $_ is not a tab separated file\n";
			print "Maybe you saved it as excel binary file\n";
			next;
		}

		my %parsed_input = parse($_);
		foreach(keys %parsed_input){
			$full_data{$_} = $parsed_input{$_};
		}
	}
	return %full_data;
}

sub parse{
	my ($filename,) = @_;
	my %data;
	
	my @params = ('unmodified_sequence', 'modified_sequence','z',
			      'actual_mass','deltamass',
				  'xcorr','deltacn','sp','rsp', 'd',
			      'ions','db_first_match','count'
			      );
	$data{'params'} = \@params;
	
	open (my $IN, '<', $filename) or die "Could not open file: $!\n";
	my $first_line = 0;
	while(<$IN>){
		chomp;
		if($first_line == 0){
			$first_line = 1;
			next;
		}

		my ($file, $scan1, $scan2, $ms, $reference, $actual_mass, $deltamass, $z, $peptide,
			$prob, $xcorr, $deltacn, $sp, $rsp, $ions, $count, $D) = split /\t/, $_;

		my $peptide_wo_mods = $peptide; # $peptide  ---> Q(737,23)FAPEY(21)EKIANILK
		$peptide_wo_mods =~ s/\(\d+(,\d+)*\)//g;

		my $spectrum = $file . '.' . $scan1 . '.' . $scan2;
		$data{$spectrum}{'ms'} = $ms;
		$data{$spectrum}{'unmodified_sequence'} = $peptide_wo_mods;
		$data{$spectrum}{'modified_sequence'} = $peptide;
		$data{$spectrum}{'z'} = $z;
		$data{$spectrum}{'actual_mass'} = $actual_mass;    # expected monoisotopic M+H
		$data{$spectrum}{'deltamass'} = $deltamass;
		$data{$spectrum}{'xcorr'} = $xcorr;
		$data{$spectrum}{'deltacn'} = $deltacn;
		$data{$spectrum}{'sp'} = $sp;
		$data{$spectrum}{'rsp'} = $rsp;
		$data{$spectrum}{'ions'} = $ions;
		$data{$spectrum}{'d'} = $D;
		$data{$spectrum}{'db_first_match'} = (split /\|/, $reference)[1];   # $reference --> sp|P13667|PDIA4_HUMAN
		$data{$spectrum}{'count'} = $count;
	}
	close $IN;
	return %data;
}

1;  #63


__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
