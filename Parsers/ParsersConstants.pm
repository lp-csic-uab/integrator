package Parsers::ParsersConstants;
use strict;
use warnings;

our $VERSION = '1.00';
our @EXPORT = qw(get_file_extensions);
use base qw(Exporter);


sub get_file_extensions{
	my ($engine, ) = @_;

	my $xls = "excel 2000 (*.xls)|*.xls";
    my $xlsx = "excel 2007+ (*.xlsx)|*.xlsx";
    my $txt = "discoverer (*.txt)|*.txt";
	my $tsv1 = "tsv (*.tsv)|*.*sv";
	my $tsv2 = "tsv (*.xls)|*.xls";
	my $zip = "zip (*.zip)|*.zip";
	my $all = "All files (*.*)|*.*";

	my %extensions = (Sequest     => ['tsv text (.xls)', "$tsv2|$tsv1|$all"],
                      Discoverer  => ['tsv text (.txt)', "$txt|$all"],
					  Omssa       => ['tsv text (.xls)', "$tsv2|$tsv1|$all"],
                      Phenyx      => ['excel 2000 (.xls)', "$xls|$all"],
				      EasyProt    => ['excel 2007+ (.xlsx)', "$xlsx|$all"],
	                  Peaks       => ['.zip', "$zip|$all"],
				      None        => ['None', $all]
					  );

	return $extensions{$engine};
}


return 1;      #32

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
