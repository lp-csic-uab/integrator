package Parsers::Peaks;

use strict;
use warnings;

my $VERSION = '1.00';
our @EXPORT_OK = qw(analyze);
use base qw(Exporter);
use Archive::Zip  qw/ :ERROR_CODES :CONSTANTS /;
use Data::Dumper;

sub analyze{
	my ($input_zipfiles, $mgf_list) = @_;

	my @input_zipfiles = @{$input_zipfiles};
	my @mgf_list = @{$mgf_list};
	my %full_data;
	my @extracted_files;

	# Build a translator dictionary to set correct filename
	my %name_translator = get_second_scan_from_mgfs(@mgf_list);

	# Parse all zip files to be joined.
	# Each zip file can contain several directories/files
	foreach (@input_zipfiles){
		my $zip = Archive::Zip->new();
		unless ( $zip->read( $_ ) == AZ_OK ) { die 'read error';}

		my @files = $zip->memberNames();

		foreach (@files){
			# There are several cases.
			#
			## In old Tests ##
			# files (TITLE)-> Cvs15minvs2h_OGE_PS_Fr07
			# mgf          -> uCvs15minvs2h_OGE_PS_Fr07.RAW.MS2.mgf
			# peaks zip    -> Cvs15minvs2h_OGE_PS_Fr07.RAW.MS3_PEAKS_3.zip
			# zip folder   -> Cvs15minvs2h_OGE_PS_Fr07.RAW.MS3_PEAKS_3
			#
			## In new Tests ##
			# files (TITLE)-> 29_IMAC_PI_0_15_120_20120110_Fr10
			# mgf          -> 2ms21_29_IMAC_PI_0_15_120_20120110_Fr10.mgf
			# peaks zip    -> 2ms21_29_IMAC_PI_0_15_120_20120110_Fr10_PEAKS_3.zip
			# zip folder   -> 2ms21_29_IMAC_PI_0_15_120_20120110_Fr10_PEAKS_3
			#
		    ## Montse standard document ##
			# files (TITLE)-> 29_IMAC_PI_0_15_120_20120110_Fr16
			# mgf          -> 2ms31_29_IMAC_PI_0_15_120_20120110_Fr16.mgf
			# peaks zip    -> 29_peaks_ms3_PI_0_15_120_20120110_Fr00.zip
			# zip folder   -> 2ms31_29_IMAC_PI_0_15_120_20120110_Fr16_PEAKS_3

			if ($_ =~ m/(.*_PEAKS_\d)\/protein-peptides.csv/){
				my $tempfile = $1;

				$zip->extractMember($_, $tempfile);
				push @extracted_files, $tempfile;

				my %parsed_input = parse($tempfile, %name_translator);
				foreach(keys %parsed_input){
					$full_data{$_} = $parsed_input{$_};
				}
			}
		}
	}
	# check at least one file has been read
	die("not valid PEAKS result files given") unless @extracted_files;

	unlink @extracted_files;
	$full_data{'params'} = get_params();

	return %full_data;
}


sub get_params{
	my @params = ('unmodified_sequence', 'modified_sequence', 'z',
				  'mz', 'mass','deltamass',
				  'logp',
				  'rt',
				  'pgroup','pid', 'db_first_match','unique',
				  'count','start','end','ptm'
	);
	return \@params;
}


sub parse{
	my ($filename, %name_translator) = @_;
	my %data;

	open (my $IN, '<', $filename) or die "Could not open file: $!\n"; ;

	my $ms;
	my $first_line = 0;
	# saved temporal peaks file:
	# 2ms21_29_IMAC_PI_0_15_120_20120110_Fr10_PEAKS_3
	# spectrum files:
	# 29_IMAC_PI_0_15_120_20120110_Fr10
	if ($filename =~ m/\dms(\d)\d*_(.*)_PEAKS_\d/){
	    $ms = $1;
		$filename = $2;
	}
	else{
		die("PEAKS Folder $filename in zip has not been named correctly\n")
	}

	my $counter = 0;
	while(<$IN>){
		chomp;
		if($first_line == 0){
			$first_line = 1;
			next;
		}

		my ($pgroup, $pid, $accession, $sequence, $unique,
			$logp, $mass, $deltamass, $mz, $z, $rt, $scan,
			$count, $start, $end, $ptm) = split /\,/, $_;

		my $key_name = $filename . '.' . $scan;
		my $spectrum = $name_translator{$key_name};

		# non defined spectrum due to non-existing key_name
		# maybe some of the original mgfs are not accessible
		# maybe filenames in mgf data are not coherent with zip internal folder names
		if (not defined $spectrum) {
			$counter += 1;
			if ($counter > 10) {
				print '=' x 45, "\n";
				print "      Too many not found scans\n";
				print "1.- Check you have all needed .MGF files\n";
				print "2.- Check zip internal folders are correctly named\n";
				print '=' x 45, "\n";
				die('Aborted by Master of the Universe');
			}
			else{
				print "$key_name key can not find its spectrum\n";
				next;
			}
		}

		($data{$spectrum}{'unmodified_sequence'},
		 $data{$spectrum}{'modified_sequence'}
		) = translate_sequence($sequence);

		$data{$spectrum}{'pgroup'} = $pgroup;
		$data{$spectrum}{'pid'} = $pid;
		$data{$spectrum}{'db_first_match'} = (split /\|/, $accession)[0];
		$data{$spectrum}{'unique'} = $unique;
		$data{$spectrum}{'logp'} = $logp;
		$data{$spectrum}{'mass'} = $mass;
		$data{$spectrum}{'deltamass'} = $deltamass;
		$data{$spectrum}{'mz'} = $mz;
		$data{$spectrum}{'z'} = $z;
		$data{$spectrum}{'rt'} = $rt;
		$data{$spectrum}{'ms'} = $ms;
		#TODO: check meaning of count
		$data{$spectrum}{'count'} = $count;
		$data{$spectrum}{'start'} = $start;
		$data{$spectrum}{'end'} = $end;
		$data{$spectrum}{'ptm'} = $ptm;
	}
	close $IN;
	return %data;
}

sub translate_sequence{
	my $sequence = shift;

	# $sequence --> "K.G(+229.16)GVTGS(-18.01)PEASISGSK(+229.16).G"   (Peaks)
	my $sequence_unmod;
	($sequence_unmod = $sequence) =~ s/\(.{1,7}\)//g;

	if($sequence =~ m/\(/){
	    # $sequence =~ s/\(\+57\.02\)/\(4\)/g;  # carbamidometilated by default
		$sequence =~ s/\(\+57\.02\)//g;         # carbamidometilated by default
	    $sequence =~ s/\(\+15\.99\)/\(35\)/g;
	    $sequence =~ s/\(\+79\.97\)/\(21\)/g;
	    $sequence =~ s/\(\-18\.01\)/\(23\)/g;
	    $sequence =~ s/\[iTRAQ_K\]/\(214\)/g;
	    $sequence =~ s/\(\+229\.16\)/\(737\)/g;
	}

	# remove extreme aminoacids
	my @unmod = (split '\.', $sequence_unmod);
	my @mod = (split '\.', $sequence);
	if (@mod == 3 or length($mod[0]) == 1) {
		$sequence_unmod = $unmod[1];
		$sequence = $mod[1];
	}
	else{
		$sequence_unmod = $unmod[0];
		$sequence = $mod[0];
	}

	# reposition Nterm modification from left to right of the first aa
	$sequence =~ s/^\(214\)(.)/$1\(214\)/;
	$sequence =~ s/^\(737\)(.)/$1\(737\)/;

	# unify when two consecutive modifications: (x)(y)->(x,y)
	$sequence =~ s/^(\w)\((\d+)\)\((\d+)\)/$1\($2\,$3\)/;

	return ($sequence_unmod, $sequence);
}


sub get_second_scan_from_mgfs{
	#  Extract the second scan from each spectrum
	#  $mgfs_list->	reference to a list of mgf files to search for info
	#  return	->	%translate{file_startscan} = file_startscan_endscan

	my @mgfs = @_;

	my %translator;

	foreach (@mgfs){
		my %mgf_translation = parse_scans($_);
		foreach (keys %mgf_translation){
			$translator{$_} = $mgf_translation{$_};
		}
	}
	return %translator;
}


sub parse_scans{
	my ($mgf,) = @_;

	open (my $MGF, '<', $mgf) or die "Could not open file: $!\n";
	my @lines = do {local $/ = "END IONS\n\n"; <$MGF>};
	close $MGF;

	my %mgf_info;
	my $key;
	foreach my $line(@lines){
		$line =~ /BEGIN IONS\nTITLE\=(.+)\nPEPMASS\=/;
		# TITLE=29_IMAC_PI_0_15_120_20120110_Fr16,Scan:1043,MS:3,Rt:28.4827
		my $spec = (split /\,/, $1)[0];
		my $scan = (split /\,/, $1)[1];
		$scan =~ s/Scan\://;
		if($scan =~ s/\-/\./){
			my ($scan1, $scan2) = split /\./, $scan;
			$key = "$spec.$scan1";
			$spec = "$spec.$scan1.$scan2";
		}
		else{
			$key = "$spec.$scan";
			$spec = "$spec.$scan.$scan";
		}
		$mgf_info{$key} = $spec;
	}
	return %mgf_info;
}

1;  #63


__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
