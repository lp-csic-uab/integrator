package Parsers::None;

use strict;
use warnings;

my $VERSION = '1.00';
our @EXPORT_OK = qw(analyze);
use base qw(Exporter);


sub analyze{
	my ($input_files, $mgf_list) = @_;

	my %full_data;
	my @params = ();

	$full_data{'params'} = \@params;
	
	return %full_data;
}


1;


__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
