package Parsers::Phenyx;

use strict;
use warnings;

my $VERSION = '1.00';
our @EXPORT_OK = qw();
use base qw(Exporter);

use Spreadsheet::ParseExcel;



sub analyze{
	my ($input_files, $mgf_list) = @_;

	my @input_files = @{$input_files};
	my %full_data;

	# Parse all xls files to be joined.
	foreach(@input_files){
		my %parsed_input = parse($_);
		foreach(keys %parsed_input){
			$full_data{$_} = $parsed_input{$_};
		}
	}
	$full_data{'params'} = get_params();

	return %full_data;
}

sub get_params{
	my @params = ('unmodified_sequence', 'modified_sequence', 'z',
				  'mz', 'deltamz',
				  'zscore', 'validity',
			      'db_first_match', 'count'
	);
	return \@params;
}

sub parse{
	my $input_file = shift;            #Excel worksheet global export
	my %data;
	my @lines;

	my $parser = Spreadsheet::ParseExcel->new();
	my $workbook = $parser->parse($input_file);

	for my $sheet ($workbook->worksheets()) {
		if ($sheet->{Name} eq 'MSMS Matches'){
			my ($row_min, $row_max) = $sheet->row_range();
			my ($col_min, $col_max) = $sheet->col_range();
			for my $row ($row_min .. $row_max) {
				for my $col ($col_min .. $col_max) {
					my $cell = $sheet->get_cell($row, $col);
					$lines[$row][$col] = $cell->value() if($cell);
				}
			}
		}
	}

	# if the first column in the first row does not contain 'Compound', do not parse.
	unless($lines[0][0] =~ /^Compound/){return;}

	for (1..$#lines){
		my @cols = @{$lines[$_]};
		my $compound_info;
		if($cols[0]){$compound_info = $cols[0]} else {$compound_info = ''};
		#
		# Extract components from compound_info
		# Eritrocitos_FASP_OGE_TiO2_Fr04,Scan:1108,MS:3,Rt:1913.337
		my ($file, $scan, $ms, $rt) = split /\,/, $compound_info;
		$scan =~ s/Scan\://;
		my ($scan1, $scan2);
		if($scan =~ /\-/){                 # if contains hyphen, split
			($scan1, $scan2) = split /\-/, $scan;
		}
		else{
			($scan1, $scan2) = ($scan, $scan);
		}
		$ms =~ s/MS\://;
		$rt =~ s/Rt\://;
		$rt = sprintf("%.0f", $rt);
		my $spectrum = $file . '.' . $scan1 . '.' . $scan2;

		my $zscore;
		if($cols[8]){$zscore = $cols[8]} else {$zscore = ''};
		#Si ya existe, solo me quedo (sobreescribo con) el de zscore mayor
		if(defined $data{$spectrum}){
			if($data{$spectrum}{'zscore'} > $zscore){
				next;
			}
		}

		my $validity;
		if($cols[1]){$validity = $cols[1]} else {$validity = ''};

		my $sequence;
		if ($cols[13]){$sequence = $cols[13]} else {$sequence = ''};

		my $z;
		if ($cols[5]){$z = $cols[5]} else {$z = ''};

		my $mz;
		if ($cols[6]){$mz = $cols[6]} else {$mz = ''};

		my $deltamz;
		if($cols[7]){$deltamz = $cols[7]} else {$deltamz = ''};

		my $dbmatch;
		if($cols[12]){$dbmatch = $cols[12]} else {$dbmatch = ''};

		my @prots = split ' ', $dbmatch;
		my $first_prot = (split '%', $prots[0])[1];

		$data{$spectrum}{'ms'} = $ms;
		$data{$spectrum}{'rt'} = $rt;
		$data{$spectrum}{'validity'} = $validity;

		($data{$spectrum}{'unmodified_sequence'},
		 $data{$spectrum}{'modified_sequence'}
		) = translate_sequence($sequence);

		# remove modification in C (CAM, unimod code: 4)
		#TODO: check every parser do this when needed
		$data{$spectrum}{'modified_sequence'}=~s/\(4\)//g;
		$data{$spectrum}{'z'} = $z;
		$data{$spectrum}{'mz'} = $mz;
		$data{$spectrum}{'deltamz'} = $deltamz;
		$data{$spectrum}{'zscore'} = $zscore;
		$data{$spectrum}{'db_first_match'} = $first_prot;
		#TODO: Check meaning of 'count' is the same for each parser
		$data{$spectrum}{'count'} = 1 + $#prots;
	}
	return %data;
}

sub translate_sequence{
	my $sequence = shift;
    # $sequence --> "KITCGLE%TMTsixplex_Nterm:TMTsixplex_K:::Cys_CAM::::%sample_1%cmpd_352"
	# $sequence --> "YISPDQDLYK%:iTRAQ_Y:::::::::iTRAQ_K:%sample_1%cmpd_314"
	my $sequence_unmod = (split /\%/, $sequence)[0];
	my $modifs = (split /\%/, $sequence)[1];
	my @aas = split //, $sequence_unmod;
	push @aas, '';                           # to get the last modification
	my @mods = split ':', $modifs;

	my $sequence_modif = '';
	for(0..$#aas){
		if($mods[$_]){
			$mods[$_] =~ s/Cys_CAM/\(4\)/;
			$mods[$_] =~ s/Oxidation_M/\(35\)/;
			$mods[$_] =~ s/PHOS/\(21\)/;
			$mods[$_] =~ s/deshidratacionST/\(23\)/;
			$mods[$_] =~ s/Dehydrated_ST/\(23\)/;
			$mods[$_] =~ s/iTRAQ_K/\(214\)/;
			$mods[$_] =~ s/iTRAQ_Y/\(214\)/;
			$mods[$_] =~ s/iTRAQ_Nterm/\(214\)/;
			$mods[$_] =~ s/TMTsixplex_K/\(737\)/;
			$mods[$_] =~ s/TMTsixplex_Y/\(737\)/;
			$mods[$_] =~ s/TMTsixplex_Nterm/\(737\)/;

			$sequence_modif = $sequence_modif . $mods[$_];
		}
		$sequence_modif = $sequence_modif . $aas[$_];
	}
	# reposition Nterm modification from left to right of the first aa
	$sequence_modif =~s/^\(214\)(.)/$1\(214\)/;
	$sequence_modif =~s/^\(737\)(.)/$1\(737\)/;

	# unify when two consecutive modifications: (x)(y)->(x,y)
	$sequence_modif =~s/^(\w)\((\d+)\)\((\d+)\)/$1\($2\,$3\)/;

	return ($sequence_unmod, $sequence_modif);
}


1;   #167


__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
