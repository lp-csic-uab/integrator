package Parsers::Discoverer;

use strict;
use warnings;
use Data::Dumper;

my $VERSION = '1.00';
our @EXPORT_OK = qw(analyze);
use base qw(Exporter);



sub analyze{
    my ($input_files, $mgf_list) = @_;
    my @input_files = @{$input_files};
    my %full_data;

    # Parse all xls files to be joined.
    foreach(@input_files){
        if (-B $_) {
            print "File $_ is not a tab separated file\n";
            print "Maybe you saved it as excel binary file\n";
            next;
        }

        my %parsed_input = parse($_);
        foreach(keys %parsed_input){
            $full_data{$_} = $parsed_input{$_};
        }
    }
    return %full_data;
}

sub parse{
    my ($filename,) = @_;
    my %data;
    my $extension;

    # these will go to the xls report and json database
    my @params = ('unmodified_sequence', 'modified_sequence','z',
                  'actual_mass','deltamass',
                  'xcorr','deltacn','pep',
                  'ions','db_first_match','count'
                  );
    $data{'params'} = \@params;

    open (my $IN, '<', $filename) or die "Could not open file: $!\n";
    my $first_line = 0;
    while(<$IN>){
        chomp;
        if($first_line == 0){
            $first_line = 1;
            next;
        }
        # protein discovered txt exports have everything between apostrophes
        $_ =~ s/"//g;

        my ($conf, $sid, $node, $sequence, $usid, $amb, $protdesc, $protsnum,
            $protgrps, $protgrps_acc, $mods, $activ, $dscore, $deltacn, $rank,
            $serank, $prsisoprob, $prssiteprob, $prsbinpepscore, $qvalue, $pep,
            $decpepts, $pepts, $xcorr, $missclvgs, $isolinterference,
            $injectime, $intensity, $z, $mz, $mh, $deltamass_da,
            $deltamass_ppm, $rt, $fscan, $lscan, $ms, $ionsmatch, $matchions,
            $totalions, $sfile, $ann) = split /\t/, $_;

        # $sequence  ---> QDVcAQDsEDKGmDFEDsEDR
        my $peptide_wo_mods = uc $sequence;

        # This parser works with two different outputs from Discoverer
        # Discoverer can work with .RAW or with .mgf files.
        # In its report, it writes the full file name of the source file:
        # 'my_file.RAW'         (thermo raw file)
        # '2ms21_my_file.mgf'   (easy_mgf generated mgf)
        # This is different to other engines which report only the filename:
        # 'my_file'             (other engines)
        # to search mgfs these prefixes and suffixes must be removed
        # because the TITLE in the mgf doesnt have them.
        #
        # This only splits at the last dot:
        ($sfile, $extension) = split /\.([^\.]+$)/, $sfile;
        # in fact it returns:
        # ['all_before_last_dot', '(group)_in_split', 'empty, no second element']
        if ($extension eq 'mgf') {
            $sfile = (split /_/, $sfile, 2)[1];
        }

        my $spectrum = $sfile . '.' . $fscan . '.' . $lscan;
        $data{$spectrum}{'ms'} = substr $ms, 2;
        $data{$spectrum}{'unmodified_sequence'} = $peptide_wo_mods;
        $data{$spectrum}{'modified_sequence'} = translate_sequence($peptide_wo_mods, $mods);
        # remove modification in C (CAM, unimod code: 4)
        $data{$spectrum}{'modified_sequence'} =~ s/\(4\)//g;
        $data{$spectrum}{'z'} = $z;
        $data{$spectrum}{'actual_mass'} = $mh;    # expected monoisotopic M+H
        $data{$spectrum}{'deltamass'} = ($mh / 1000000) * $deltamass_ppm;
        $data{$spectrum}{'xcorr'} = $xcorr;
        $data{$spectrum}{'deltacn'} = $deltacn;
        $data{$spectrum}{'ions'} = $ionsmatch;
        $data{$spectrum}{'inject_time'} =  $injectime;
        $data{$spectrum}{'pep'} = $pep;            # PEP <=> D from sequest
        # $protgrps_acc --> A4D2P2;B1AH77
        $data{$spectrum}{'db_first_match'} = (split /;/, $protgrps_acc)[0];
        $data{$spectrum}{'count'} = $protsnum;
    }
    close $IN;
    return %data;
}


sub translate_sequence{
    my ($sequence, $modifs) = @_;
    # $sequence --> KRTPSPSYQR
    # $modifs --> K1(TMT6plex); N-Term(TMT6plex); T3(Phospho); Y8(Phospho)
    $modifs =~ s/\s+//g;
    $modifs =~ s/N-Term/X0/;
    my @aas = split //, $sequence;
    my @mods = split ';', $modifs;
    
    # modifications in example given by MCP on march 2015
    my %mod_convert = (Carbamidomethyl => '(4)',
                       Phospho => '(21)',
                       Oxidation => '(35)',
                       TMT6plex => '(737)'
                       );

    my %dmods;
    foreach(@mods){
        my ($t1, $t2) = split '\(', $_;
        substr($t1, 0, 1) = "";
        $t2 = substr($t2, 0, -1);
        $dmods{$t1} = $mod_convert{$t2};
    };

     my $sequence_modif = '';
    for(0..$#aas){
        if($dmods{$_}){
            $sequence_modif = $sequence_modif . $dmods{$_};
        }
        $sequence_modif = $sequence_modif . $aas[$_];
    }
    # add modif C-terminal aa
    if($dmods{$#aas + 1}){
        $sequence_modif = $sequence_modif . $dmods{$#aas + 1};
    }

    # reposition Nterm modification from left to right of the first aa
	$sequence_modif =~s/^\(214\)(.)/$1\(214\)/;
	$sequence_modif =~s/^\(737\)(.)/$1\(737\)/;

	# unify when two consecutive modifications: (x)(y)->(x,y)
	$sequence_modif =~s/^(\w)\((\d+)\)\((\d+)\)/$1\($2\,$3\)/;

    return $sequence_modif;
}

1;  #63


__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
