package Parsers::Omssa;

use strict;
use warnings;

my $VERSION = '1.00';
our @EXPORT_OK = qw();
use base 'Exporter';


sub analyze{
	my ($input_files, $mgf_list) = @_;
	my %full_data;

	my @input_files = @{$input_files};

	# Parse all xls files to be joined.
	foreach(@input_files){
        if (-B $_) {
            print "File $_ is not a tab separated file\n";
            print "Maybe you saved it as excel binary file\n";
            next;
		}
		my %parsed_input = parse($_);
		foreach(keys %parsed_input){
			$full_data{$_} = $parsed_input{$_};
		}
	}
	$full_data{'params'} = get_params();

	return %full_data;
}


sub get_params{
	my @params = ('unmodified_sequence', 'modified_sequence', 'z',
				  'calc_neutral_pep_mass', 'massdiff',
				  'pvalue', 'expect',
				  'db_first_match',
	);
	return \@params;
}


sub parse{
	my $filename = shift;
	my %data;

	open (my $IN, '<', $filename) or die "Could not open file: $!\n";
	my $first_line = 0;
	while(<$IN>){
		chomp;
		if ($first_line == 0){
			$first_line = 1;
			next;
		}

		my ($file, $scan1, $scan2, $ms, $peptide, $expect,
			$mass_exp, $gi, $accesion, $start, $stop, $defline,
			$mods, $z, $calc_neutral_pep_mass, $pvalue
			) = split /\t/, $_;

		my $spectrum = $file . '.' . $scan1 . '.' . $scan2;
		my $massdiff = $mass_exp - $calc_neutral_pep_mass;
        my $peptide_wo_mods = $peptide; # $peptide  ---> Q(737,23)FAPEY(21)EKIANILK
        $peptide_wo_mods =~ s/\(\d+(,\d+)*\)//g;

		$data{$spectrum}{'unmodified_sequence'} = $peptide_wo_mods;
		$data{$spectrum}{'modified_sequence'} = $peptide;
		$data{$spectrum}{'pvalue'} = $pvalue;
		$data{$spectrum}{'expect'} = $expect;
		$data{$spectrum}{'db_first_match'} = (split /\|/, $defline)[1];
		$data{$spectrum}{'calc_neutral_pep_mass'} = $calc_neutral_pep_mass;
		$data{$spectrum}{'massdiff'} = $massdiff;
		$data{$spectrum}{'z'} = $z;
		$data{$spectrum}{'ms'} = $ms;
	}
	close $IN;
	return %data;
}


1;           #60

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
