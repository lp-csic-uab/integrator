#!/usr/bin/perl -w
#
use strict;
use warnings;

my $ntests;
use Test::More;
use Test::Differences;

BEGIN {push @INC, "../";
       $ntests = 4;
       plan tests => $ntests;}

use Parsers::Discoverer;

print "# Testing parser's sequence translations\n";



#


my %data = (tADGIVSHLk          => ['N-Term(TMT6plex); K10(TMT6plex)',
                                    'T(737)ADGIVSHLK(737)'],
            tASFsESRADEVAPAkk   => ['N-Term(TMT6plex); S5(Phospho); K16(TMT6plex); K17(TMT6plex)',
                                    'T(737)ASFS(21)ESRADEVAPAK(737)K(737)'],
            tRHSPtPQQSNR        => ['N-Term(TMT6plex); T1(Phospho); T6(Phospho)',
                                    'T(737,21)RHSPT(21)PQQSNR'],
            vAAYDkLEk           => ['N-Term(TMT6plex); K6(TMT6plex); K9(TMT6plex)',
                                    'V(737)AAYDK(737)LEK(737)']
		   );


my $sequence;
my $mods;
my $converted;
my $result;
my $other;

my @sequences = keys(%data);

for (my $test=0; $test<4; $test++){
    $sequence = $sequences[$test];
	($mods, $converted) = @{$data{$sequence}};
    my $unmod_sequence = uc $sequence;

	$result = Parsers::Discoverer::translate_sequence($unmod_sequence, $mods);

    eq_or_diff ($result, $converted, "$result Discoverer conversion on $sequence");
}



1;
