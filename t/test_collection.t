#!/usr/bin/perl -w
use strict;
use warnings;

use Test::More tests => 9;
use Test::Differences;

BEGIN {push @INC, '../'};

use Core::Ascore;

print "# Testing Peptide_collection\n";


my %peptides = ('ALM(35)GQRS(21)TM'            =>  ['ALM(35)GQRS(21)TM',
													'ALM(35)GQRST(21)M'],

                'ALDEAGQRS(21)TM'              =>  ['ALDEAGQRS(21)TM',
													'ALDEAGQRST(21)M'],

				'S(21)NLGQRTM'                 =>  ['S(21)NLGQRTM',
													'SNLGQRT(21)M'],

				'S(214)NLGQRT(21)M'            =>  ['S(214,21)NLGQRTM',
													'S(214)NLGQRT(21)M'],

				'M(214,35)LGQRS(21)TM'         =>  ['M(214,35)LGQRS(21)TM',
													'M(214,35)LGQRST(21)M'],

				'S(21,214)NLGQRTM'             =>  ['S(214,21)NLGQRTM',
													'S(214)NLGQRT(21)M'],

				'S(214,21)NLGQRTM'             =>  ['S(214,21)NLGQRTM',
													'S(214)NLGQRT(21)M'],

				'A(737)TS(21)LPSLDTPGELR'      =>  ['A(737)T(21)SLPSLDTPGELR',
													'A(737)TS(21)LPSLDTPGELR',
													'A(737)TSLPS(21)LDTPGELR',
													'A(737)TSLPSLDT(21)PGELR'],

				'A(737)T(21)SYGS(23)LSATG'     =>  ['A(737)T(23)S(21)YGSLSATG',
													'A(737)T(23)SY(21)GSLSATG',
													'A(737)T(23)SYGS(21)LSATG',
													'A(737)T(23)SYGSLS(21)ATG',
													'A(737)T(23)SYGSLSAT(21)G',

													'A(737)T(21)S(23)YGSLSATG',
													'A(737)TS(23)Y(21)GSLSATG',
													'A(737)TS(23)YGS(21)LSATG',
													'A(737)TS(23)YGSLS(21)ATG',
													'A(737)TS(23)YGSLSAT(21)G',

													'A(737)T(21)SYGS(23)LSATG',
													'A(737)TS(21)YGS(23)LSATG',
													'A(737)TSY(21)GS(23)LSATG',
													'A(737)TSYGS(23)LS(21)ATG',
													'A(737)TSYGS(23)LSAT(21)G',

													'A(737)T(21)SYGSLS(23)ATG',
													'A(737)TS(21)YGSLS(23)ATG',
													'A(737)TSY(21)GSLS(23)ATG',
													'A(737)TSYGS(21)LS(23)ATG',
													'A(737)TSYGSLS(23)AT(21)G',

													'A(737)T(21)SYGSLSAT(23)G',
													'A(737)TS(21)YGSLSAT(23)G',
													'A(737)TSY(21)GSLSAT(23)G',
													'A(737)TSYGSLS(21)AT(23)G',
													'A(737)TSYGS(21)LSAT(23)G',
													  ]

				);


foreach (keys %peptides){
    my @collection = sort (Core::Ascore::peptide_collection($_));
    my @expected = sort @{$peptides{$_}};
	eq_or_diff(\@collection, \@expected, "combinations of $_");
}

1;
