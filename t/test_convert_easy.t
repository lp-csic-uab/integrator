#!/usr/bin/perl -w
#
use strict;
use warnings;

my $ntests;
use Test::More;
use Test::Differences;

BEGIN {push @INC, "../";
       $ntests = 3+3+3;
       plan tests => $ntests;}

use Parsers::EasyProt;

print "# Testing parser's sequence translations\n";

# Sequence														       Conversion
# [TMTsixplex_Nterm]AGTLSITEFADMLSGNAGGFRS[deshidratacionST]R		   A(737)GTLSITEFADMLSGNAGGFRS(23)R
# AGTLSITEFADMLSGNAGGFRS                                               AGTLSITEFADMLSGNAGGFRS
# [TMTsixplex_Nterm]K[TMTsixplex_K]LS[PHOS]EMQDLEETMAK[TMTsixplex_K]   K(737,737)LS(21)EMQDLEETMAK(737)

#
my %easy_prot = ('[TMTsixplex_Nterm]AGTLSITEFADMLSGNAGGFRS[deshidratacionST]R'        => 'A(737)GTLSITEFADMLSGNAGGFRS(23)R',
				 'AGTLSITEFADMLSGNAGGFRS'                                             => 'AGTLSITEFADMLSGNAGGFRS',
				 '[TMTsixplex_Nterm]K[TMTsixplex_K]LS[PHOS]EMQDLEETMAK[TMTsixplex_K]' => 'K(737,737)LS(21)EMQDLEETMAK(737)'
                );

my $sequence;
my $converted;
my $result;
my $other;

my @sequences = keys(%easy_prot);

for (my $test=0; $test<3; $test++){
    $sequence = $sequences[$test];
	$converted = $easy_prot{$sequence};

	($other, $result) = Parsers::EasyProt::translate_sequence($sequence);

    eq_or_diff ($result, $converted, "EasyProt conversion on $sequence");
}

1;
