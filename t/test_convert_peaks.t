#!/usr/bin/perl -w
#
use strict;
use warnings;

my $ntests;
use Test::More;
use Test::Differences;

BEGIN {push @INC, "../";
       $ntests = 3+3+3;
       plan tests => $ntests;}

use Parsers::Peaks;

print "# Testing parser's sequence translations\n";

# Sequence														       Conversion
# [TMTsixplex_Nterm]AGTLSITEFADMLSGNAGGFRS[deshidratacionST]R		   A(737)GTLSITEFADMLSGNAGGFRS(23)R
# AGTLSITEFADMLSGNAGGFRS                                               AGTLSITEFADMLSGNAGGFRS
# [TMTsixplex_Nterm]K[TMTsixplex_K]LS[PHOS]EMQDLEETMAK[TMTsixplex_K]   K(737,737)LS(21)EMQDLEETMAK(737)

#


my %peaks   =  ('R.T(+229.16)(+79.97)VIT(+79.97)PDPNLS(+79.97)IDQVGVPR.S' => 'T(737,21)VIT(21)PDPNLS(21)IDQVGVPR',
               'R.AGTLSITEFADMLSGNAGGFRS.K'                                  => 'AGTLSITEFADMLSGNAGGFRS',
			   'R.S(+229.16)(-18.01)PVSSSK(+229.16).G'                   => 'S(737,23)PVSSSK(737)'
			   );

my $sequence;
my $converted;
my $result;
my $other;


@sequences = keys(%peaks);

for (my $test=0; $test<3; $test++){
    $sequence = $sequences[$test];
	$converted = $peaks{$sequence};

	($other, $result) = Parsers::Peaks::translate_sequence($sequence);

    eq_or_diff ($result, $converted, "Phenyx conversion on $sequence");

}

1;
