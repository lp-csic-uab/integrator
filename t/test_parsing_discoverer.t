#!/usr/bin/perl -w
#
use strict;
use warnings;

my $ntests;
use Test::More;
use Test::Differences;
use Data::Dumper;
use File::Slurp qw(read_file write_file);

use JSON::XS;

BEGIN {push @INC, "../";
       $ntests = 1;
       plan tests => $ntests;}

use Parsers::Discoverer;

#
my $saving = 1;
if ($saving){
	print "# Saving new result json file for testing\n";
}
else{
	print "# Testing file parsing\n";
}

my $dir = 'C:/Perl/perlgramas/_integrator/Test/TestData/test_Parsers/Discoverer/';

my @dfiles = ($dir . 'discoverer_29_IMAC_PI_Fr28_red.txt', );
my @mgf_list = ($dir . '2ms21_29_IMAC_PI_0_15_120_20120110_Fr28_red.mgf', );
my $resultfile = $dir . 'discoverer_test_output.json';

my $expected_ref;

if (! $saving) {$expected_ref = read_json($resultfile);}

for (my $test=0; $test<1; $test++){
 	my %result = Parsers::Discoverer::analyze(\@dfiles, \@mgf_list);

	if ($saving){
		saving_tool(%result);
		print "# file $resultfile saved\n\n";
	}
    else{
		eq_or_diff (\%result, $expected_ref, "Peaks conversion");
	}
}


sub saving_tool{
	my (%data,) = @_;
	my $json_text = JSON::XS->new->utf8->pretty->encode (\%data);
	#my $json_text = encode_json (\%data);
	write_file($resultfile, { binmode => ':raw' }, $json_text);
}

sub read_json{
	my ($file, ) = @_;
	my $json_text = read_file($file, { binmode => ':raw' });
	my $data_ref = JSON::XS->new->utf8->pretty->decode ($json_text);
	#my $data_ref = decode_json $json_text;
	return $data_ref;
}

1;