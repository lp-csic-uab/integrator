#!/usr/bin/perl -w
use strict;
use warnings;

use Test::More tests => 10;
use Test::Differences;

BEGIN {push @INC, "../"};

use Core::Integration qw(calculate_mz);

print "# Testing Calc_mz\n";

                                                        #OMSSA Calc Neutral Pep Mass
my %peptides = ("LSS(23)SWESLQPEPSHPY"           	=>   '1824.83187',
                "M(35)LDQTLLDLNEM(35)"           	=>   '1466.66328',
                "NLALDEAGQRS(21)TM"              	=>   '1484.63306',
                "A(214)LQQQQQQQQQK(214)"         	=>   '1770.95804',
                "L(214)QDSSDPDTGSEEEGS(23)SRLSPPHSPR" => '2892.32435',
                "Q(214)QYESVAAK(214)"            	=>   '1310.70740',
                "I(737)AQSDYIPTQQDVLR"           	=>   '1975.05774',
                "G(737)S(23)VSDEEMMELR"          	=>   '1592.73773',
				"S(737,23)GVSDEEMMELR"          	=>   '1592.73773', #do not pass
                "R(737)LSQPES(23)AEK(737)"       	=>   '1583.90369',

                );


my $masa;
my $mz;
my $expected;
foreach(keys %peptides){
    $mz = calculate_mz($_, 1);
	$masa = sprintf("%.5f", ($mz - 1.007825) ); #
    $expected = $peptides{$_};
	is ($masa, $expected, "mass of $_");
}

1;
