#!/usr/bin/perl -w
use strict;
use warnings;

use Test::More tests => 6;
use Test::Differences;

BEGIN {push @INC, '../'};

require 'collection_dev.pl';

print "# Testing Peptide_collection\n";


my %peptides = (
                'NLALDEAGQRS(21)TM'              	=>   ['NLALDEAGQRS(21)TM',
														  'NLALDEAGQRST(21)M'],
                'S(21)NLALDEAGQRTM'              	=>   ['S(21)NLALDEAGQRTM',
														  'SNLALDEAGQRT(21)M'],
				'S(214)NLALDEAGQRT(21)M'            =>   ['S(214,21)NLALDEAGQRTM',
														  'S(214)NLALDEAGQRT(21)M'],

				'M(214,35)LALDEAGQRS(21)TM'       	=>   ['M(214,35)LALDEAGQRS(21)TM',
														  'M(214,35)LALDEAGQRST(21)M'],
				'S(21,214)NLALDEAGQRTM'             =>   ['S(214,21)NLALDEAGQRTM',
														  'S(214)NLALDEAGQRT(21)M'],
				'S(214,21)NLALDEAGQRTM'             =>   ['S(214,21)NLALDEAGQRTM',
														  'S(214)NLALDEAGQRT(21)M']
				);


foreach (keys %peptides){
    my @collection = sort (peptide_collection_2($_));
    my @expected = sort @{$peptides{$_}};
	eq_or_diff(\@collection, \@expected, "combinations of $_");
}

1;
