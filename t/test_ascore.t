#!/usr/bin/perl -w
#
#Note this doesnt work outside of TestHarness
use strict;
use warnings;

my $ntests;
use Test::More;
use Test::Differences;

BEGIN {push @INC, "../";
       $ntests = 7;
       plan tests => $ntests;}

use Core::Ascore qw(ascore);
use Core::Integration qw(extract_spectra calculate_mz);

print "# Testing Ascore\n";

# Espectra                                 MS	Z    sec                       				ascore1		ascore2   ascore3
#Cvs15minvs2h_OGE_PS_Fr01.830.831		   2	2	A(737)TSSSSGS(21)LSATGR					20.628		-         -
#Cvs15minvs2h_OGE_PS_Fr07.766.767		   2	3	V(737)DALGT(21)PYSSS(21)MRLERK			22.375		37.818    -
#Lymphos_iTRAQ_1mg_TiO2_Fr46.1675.1678	   2	3	TGK(214)EY(214)IPGQPPLS(21)QSSDSSPTR	 4.753		-         -
#Cvs15minvs2h_OGE_PS_Fr11.1548.1548        3    3   S(737,23)LSRPSSLIEQEK(737)               7.1666     -         -
#Cvs15minvs2h_OGE_PS_Fr07.2101.2101	       3    3   V(737)S(23)KPVELHAS(23)IHDNK(737)	  1000.0000	   999.0000   -
#Eritros_precips_OGE_PS_Fr08.1432.1432     2    3   GVAAPAPLHAS(21)AT(21)QVAQEMT(21)PSK     93.74       92.86   33.18



my $precision = 0.5;
my $limit_mz_sup = 2000;
my $limit_mz_inf = 200;
#
my %expected =  ('A(737)TSSSSGS(21)LSATGR'	            => ['A(737)TSSSSGS(21)LSATGR', '20.6284'],
				 'V(737)DALGT(21)PYSSS(21)MRLERK'       => ['V(737)DALGT(21)PYSSS(21)MRLERK', '22.3746', '37.8176'],
				 'TGK(214)EY(214)IPGQPPLSQSSDSS(21)PTR' => ['TGK(214)EY(214)IPGQPPLS(21)QSSDSSPTR', '4.7527'],
                 'S(737,23)LSRPSSLIEQEK(737)'           => ['S(737,23)LSRPSSLIEQEK(737)', '7.1666'],
                 'S(737)LSRPSS(23)LIEQEK(737)'          => ['S(737,23)LSRPSSLIEQEK(737)', '7.1666'],
                 'V(737)S(23)KPVELHAS(23)IHDNK(737)'    => ['V(737)S(23)KPVELHAS(23)IHDNK(737)', '1000.0000', '999.0000'],
                 'GVAAPAPLHAS(21)AT(21)QVAQEMT(21)PSK'  => ['GVAAPAPLHAS(21)AT(21)QVAQEMT(21)PSK', '92.5205', '92.8568', '33.1763']
                );
#
my @z = (2, 3, 3, 3, 3, 3, 3);
#
my @mgfs_filelist = ('t/test_ascore/mgf/uCvs15minvs2h_OGE_PS_Fr01.RAW.MS2.mgf',
					 't/test_ascore/mgf/uCvs15minvs2h_OGE_PS_Fr07.RAW.MS2.mgf',
					 't/test_ascore/mgf/Lymphos_iTRAQ_1mg_TiO2_Fr46.RAW.MS2.mgf',
                     't/test_ascore/mgf/Cvs15minvs2h_OGE_PS_Fr11.RAW.MS3.mgf',
                     't/test_ascore/mgf/Cvs15minvs2h_OGE_PS_Fr11.RAW.MS3.mgf',
                     't/test_ascore/mgf/Cvs15minvs2h_OGE_PS_Fr07.RAW.MS3.mgf',
                     't/test_ascore/mgf/Eritros_precips_OGE_PS_Fr08.RAW.MS2.mgf'
					 );
#
my @specs_list = ('Cvs15minvs2h_OGE_PS_Fr01.830.831',
				  'Cvs15minvs2h_OGE_PS_Fr07.766.767',
                  'Lymphos_iTRAQ_1mg_TiO2_Fr46.1675.1678',
                  'Cvs15minvs2h_OGE_PS_Fr11.1548.1548',
                  'Cvs15minvs2h_OGE_PS_Fr11.1548.1548',
                  'Cvs15minvs2h_OGE_PS_Fr07.2101.2101',
                  'Eritros_precips_OGE_PS_Fr08.1432.1432'
				  );
#
my @peptide = ('A(737)TSSSSGS(21)LSATGR',
			   'V(737)DALGT(21)PYSSS(21)MRLERK',
			   'TGK(214)EY(214)IPGQPPLSQSSDSS(21)PTR',
               'S(737,23)LSRPSSLIEQEK(737)',
               'S(737)LSRPSS(23)LIEQEK(737)',
               'V(737)S(23)KPVELHAS(23)IHDNK(737)',
               'GVAAPAPLHAS(21)AT(21)QVAQEMT(21)PSK'
			   );
#
my %specs = extract_spectra(\@specs_list, \@mgfs_filelist);
#
my $spec;
my $parent_mass;
my $tmasses;
my $intensities;
my %results;

for (my $test=0; $test<$ntests; $test++){
    my @rdata;
    $spec = $specs_list[$test];
	$parent_mass = $specs{$spec}{'parent'};
	$tmasses = $specs{$spec}{'masses'};
	$intensities = $specs{$spec}{'intensities'};

	%results = ascore($peptide[$test], $parent_mass, $z[$test], $tmasses,
						$intensities, $precision, $limit_mz_sup, $limit_mz_inf);

    push @rdata, $results{'peptide'};
    foreach (@{$results{'ascore'}}) {push @rdata, sprintf("%.4f", $_)};
    eq_or_diff (\@rdata, @expected{$peptide[$test]}, "ascore of $peptide[$test]");

}

1;
