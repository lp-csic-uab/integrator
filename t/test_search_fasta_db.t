#!/usr/bin/perl -w
#
use strict;
use warnings;

my $ntests;
use Test::More;
use Test::Differences;

BEGIN {push @INC, "../";
       $ntests = 2;
       plan tests => $ntests;}

use Core::Integration qw(search_fasta_db);

print "# Testing Integration->search_fasta_db\n";

my $fasta = 'C:\Perl\perlgramas\_integrator\Test\TestData\uniprot\rev_uniprot_sprot_15_12_isoform_human.fasta';
#
my %expected =  ('TIAPALVSK'    => ['TIAPALVSK',
									'P06733',
									'P06733',
									'ENOA_HUMAN Alpha-enolase'],
				 'VIGSGCNLDSAR' => ['VIGSGCNLDSAR',
									'P00338|P00338-2|P07195|P07864|Q6ZMR3',
									'P00338',
									'LDHA_HUMAN L-lactate dehydrogenase A chain'],
                );
#
my @peptides = ('TIAPALVSK', 'VIGSGCNLDSAR',
			   );
#
my $hits = search_fasta_db($fasta, @peptides);
my %hits = %{$hits};
#
foreach (@peptides){
    my $pept  = $hits{$_}{'seq'};
	my $prots = $hits{$_}{'prots'};
	my $prot  = $hits{$_}{'first_prot'};
	my $name  = $hits{$_}{'first_prot_name'};

    eq_or_diff ([$pept, $prots, $prot, $name], \@{$expected{$_}}, "expected of $_");
}

1;
