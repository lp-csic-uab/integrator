package Reports::ExtReport;

use strict;
use warnings;
use Data::Dumper;

my $VERSION = '1.01';
our @EXPORT_OK = qw(gen_ext_report);
use base qw(Exporter);

sub gen_ext_report{
	my ($data_ref, $ext_report_file, $limit_ascore, $is_tmt_or_itraq) = @_;

	my %data = %{$data_ref};

	my $fst_engine = delete $data{'fst_engine'};
	my $snd_engine = delete $data{'snd_engine'};
	my $trd_engine = delete $data{'trd_engine'};
	my $fst_params = delete $data{'fst_params'};
	my $snd_params = delete $data{'snd_params'};
	my $trd_params = delete $data{'trd_params'};

	if (not defined $fst_params){
		my @f = ();
		$fst_params = \@f;
	}

	if (not defined $snd_params){
		my @s = ();
		$snd_params = \@s;
	}

	if (not defined $trd_params){
		my @t = ();
		$trd_params = \@t;
	}

	my $itraq_columns = 4;
	my $tmt_columns = 6;
	my $ascore_columns = 4;
	my $number_ascores = 3;

	# consensus headings
	my $cons_head_txt = "unmodified peptide\tmodified peptide\t" .
					    "z\tdelta_mz\tproteins\t" .
					    "first accession\tfirst name\tphosphorylated\t_\t";
	my $cons_tail_txt = "ascore revised peptide\tascore1\tascore2\tascore3\t_";
	my $cons_mid_txt;

	if ($is_tmt_or_itraq eq "itraq"){
		$cons_mid_txt =  "itraq114\titraq115\titraq116\titraq117\t_\t";
	}
	elsif ($is_tmt_or_itraq eq "tmt"){
		$cons_mid_txt = "TMT_126\tTMT_127\tTMT_128\tTMT_129\tTMT_130\tTMT_131\t_\t"
	}
	else{
		$cons_mid_txt = "_\t";
	}

	my $cons_txt = $cons_head_txt . $cons_mid_txt . $cons_tail_txt;

	# specific headings
	my @engine_acronims = (substr($fst_engine, 0, 2),
						   substr($snd_engine, 0, 2),
						   substr($trd_engine, 0, 2)
	);
	my $fst_string = join("\t", @$fst_params);
	my $snd_string = join("\t", @$snd_params);
	my $trd_string = join("\t", @$trd_params);

	open (my $GLOBAL, '>', $ext_report_file) or die "Could not open file: $!\n";

	# print heading 1st row
	print $GLOBAL "\t" x (5 + 3 + 1);
	print $GLOBAL "$fst_engine\t" x @$fst_params;
	print $GLOBAL "_\t" ;
	print $GLOBAL "$snd_engine\t" x @$snd_params;
	print $GLOBAL "_\t" ;
	print $GLOBAL "$trd_engine\t" x @$trd_params;
	print $GLOBAL "_\t" ;
	print $GLOBAL "consensus\t" x 7;
	print $GLOBAL "\n";

	# print heading 2nd row
	print $GLOBAL "file\tfirst scan\tlast scan\tMS\t#engines\t";
	print $GLOBAL join("\t", @engine_acronims);
	print $GLOBAL "\tRejected pept\t";
	print $GLOBAL "$fst_string\t" x (@$fst_params > 0);
	print $GLOBAL "_\t" ;
	print $GLOBAL "$snd_string\t" x (@$snd_params > 0);
	print $GLOBAL "_\t" ;
	print $GLOBAL "$trd_string\t" x (@$trd_params > 0);
	print $GLOBAL "_\t" ;
	print $GLOBAL $cons_txt;
	print $GLOBAL "\n";

	# print data rows
	foreach my $spectrum(keys %data){
		# Only report consensus in global report !!
		unless(defined $data{$spectrum}{'consensus_mod'}){
			next;
		}
		my $phospho = 0;
		my $pept_conteo_fosfs = $data{$spectrum}{'consensus_mod'};
		if ($pept_conteo_fosfs =~ /(\(21\)|\(21\,|21\)|\(23\)|\(23\,|23\))/) {
			$phospho = 1;
		}

		if($phospho){
			my ($a1,$a2,$a3) = (0, 0, 0);
			if (defined $data{$spectrum}{'ascore_data'}){
				if (defined $data{$spectrum}{'ascore_data'}{'ascore'}[0]){
					if($data{$spectrum}{'ascore_data'}{'ascore'}[0] > $limit_ascore){
						$a1 = 1;
					}
				}
				else{
					$a1 = 6666;
				}
				if (defined $data{$spectrum}{'ascore_data'}{'ascore'}[1]){
					if($data{$spectrum}{'ascore_data'}{'ascore'}[1] > $limit_ascore){
						$a2 = 1;
					}
				}
				if (defined $data{$spectrum}{'ascore_data'}{'ascore'}[2]){
					if($data{$spectrum}{'ascore_data'}{'ascore'}[2] > $limit_ascore){
						$a3 = 1;
					}
				}
			}
		}
		my $dummy = $spectrum;
		$dummy =~ s/\./\t/g;
		print $GLOBAL $dummy, "\t";
		print $GLOBAL $data{$spectrum}{'ms'}, "\t";
        #
		my ($fst, $snd, $trd) = (0, 0, 0);
		my $engines = 0;
		my $rejected_peptide = '';
        my $unmod;
		my $consensus;
		# Note 'J'
		# modified_sequence   = 'V(737)EILANDQGNR'
		# unmodified_sequence = 'VEILANDQGNR'
		# consensus           = 'VEIJANDQGNR'
		($consensus = $data{$spectrum}{'consensus'}) =~ s/[LI]/J/g;;
		if (defined $data{$spectrum}{$fst_engine}{'unmodified_sequence'}){
			#print $spectrum, "\n";
			#print $data{$spectrum}{'consensus'}, "\n";
			($unmod = $data{$spectrum}{$fst_engine}{'unmodified_sequence'})
			=~ s/[LI]/J/g;
			if ($unmod eq $consensus){
				$fst = 1;
			}
			else{
				$rejected_peptide
				= $data{$spectrum}{$fst_engine}{'modified_sequence'};
			}
		}
		if (defined $data{$spectrum}{$snd_engine}{'unmodified_sequence'}){
			($unmod = $data{$spectrum}{$snd_engine}{'unmodified_sequence'})
			=~ s/[LI]/J/g;
			if ($unmod eq $consensus){
				$snd = 1;
			}
			else{
				$rejected_peptide
				= $data{$spectrum}{$snd_engine}{'modified_sequence'};
			}
		}
		if (defined $data{$spectrum}{$trd_engine}{'unmodified_sequence'}){
			($unmod = $data{$spectrum}{$trd_engine}{'unmodified_sequence'})
			=~ s/[LI]/J/g;
			if ($unmod eq $consensus){
				$trd = 1;
			}
			else{
				$rejected_peptide
				= $data{$spectrum}{$trd_engine}{'modified_sequence'};
			}
		}
		$engines = $fst + $snd + $trd;
		print $GLOBAL "$engines\t$fst\t$snd\t$trd\t$rejected_peptide\t";
		#
		if ($fst){
			foreach (@$fst_params){
				print $GLOBAL $data{$spectrum}{$fst_engine}{$_}, "\t";
			};
		}
		else{
			foreach (@$fst_params){
				print $GLOBAL "\t";
			}
		}
		print $GLOBAL "+\t";

		if ($snd) {
			foreach (@$snd_params){
				print $GLOBAL $data{$spectrum}{$snd_engine}{$_}, "\t";
			};
		}
		else{
			foreach (@$snd_params){
				print $GLOBAL "\t";
			}
		}
		print $GLOBAL "+\t";

		if ($trd){
			foreach (@$trd_params){
				print $GLOBAL $data{$spectrum}{$trd_engine}{$_}, "\t";
			};
		}
		else{
			foreach (@$trd_params){
				print $GLOBAL "\t";
			}
		}
		print $GLOBAL "+\t";

		print $GLOBAL $data{$spectrum}{'consensus'}, "\t";
		print $GLOBAL $data{$spectrum}{'consensus_mod'}, "\t";
		print $GLOBAL $data{$spectrum}{'consensus_z'}, "\t";
		if(defined $data{$spectrum}{'consensus_deltamz'}){
			print $GLOBAL $data{$spectrum}{'consensus_deltamz'}, "\t";
		}
		else{
			print $GLOBAL "0\t";
		}

		print $GLOBAL $data{$spectrum}{'consensus_proteins'},"\t";
		print $GLOBAL $data{$spectrum}{'consensus_first_prot'},"\t";
		print $GLOBAL $data{$spectrum}{'consensus_first_prot_name'},"\t";
		print $GLOBAL $phospho,"\t+\t";

		# quantification data
		if ($is_tmt_or_itraq eq "itraq"){
			if (defined $data{$spectrum}{'itraq_info'}){
				my $itraq_info = $data{$spectrum}{'itraq_info'};
				my ($i114, $i115, $i116, $i117);
				if(($i114, $i115, $i116, $i117) = split /\|/, $itraq_info){
					print $GLOBAL "$i114\t$i115\t$i116\t$i117\t";
				}
				else {
					print $GLOBAL "0\t" x $itraq_columns;
				}
			}
			else {
				print $GLOBAL "0\t" x $itraq_columns;
			}
			print $GLOBAL "+\t";
		}
		elsif ($is_tmt_or_itraq eq "tmt"){
			if(defined $data{$spectrum}{'tmt_info'}){
				my $tmt_info = $data{$spectrum}{'tmt_info'};
				my ($t126, $t127, $t128, $t129, $t130, $t131);
				if(($t126, $t127, $t128, $t129, $t130, $t131) =
				   split /\|/, $tmt_info){
					print $GLOBAL "$t126\t$t127\t$t128\t$t129\t$t130\t$t131\t";
				}
				else{
					print $GLOBAL "0\t" x $tmt_columns;
				}
			}
			else{
				print $GLOBAL "0\t" x $tmt_columns;
			}
			print $GLOBAL "+\t";
		}
		else{
			print $GLOBAL "+\t";
		}

		my $cuenta_ascores = 0;
		if (defined $data{$spectrum}{'ascore_data'}{'ascore'}){
			print $GLOBAL $data{$spectrum}{'ascore_data'}{'peptide'}, "\t";
			my @ascores = @{$data{$spectrum}{'ascore_data'}{'ascore'}};
			foreach(@ascores){
				$cuenta_ascores++;
				print $GLOBAL sprintf("%.2f", $_), "\t";
			}
			for (my $t=0; $t<($number_ascores-$cuenta_ascores); $t++){
				print $GLOBAL "\.\t";
			}
		}
		else{
			if (defined $data{$spectrum}{'consensus_mod'}){
				if ($data{$spectrum}{'consensus_mod'} =~ /(\(21\)|\(23\))/){
					print $GLOBAL $data{$spectrum}{'consensus_mod'}, "\.\t" x $ascore_columns;
				}
				else{
					print $GLOBAL "\.\t" x $ascore_columns;
				}
			}
			else{
				print $GLOBAL "\.\t" x $ascore_columns;
			}
		}
		print $GLOBAL "+\n";
	}
	close $GLOBAL;
	return 1;
}

1;            #282

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
