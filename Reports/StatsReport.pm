package Reports::StatsReport;

use utf8;
use strict;
use warnings;

my $VERSION = '1.00';
our @EXPORT_OK = qw(gen_stats_report);
use base qw(Exporter);
use Carp qw(croak);


sub gen_stats_report{
	my %data  = %{$_[0]};
	my $extended_report_file = $_[1];
	my $limit_ascore = $_[2];

	my $fst_engine = delete $data{'fst_engine'};
	my $snd_engine = delete $data{'snd_engine'};
	my $trd_engine = delete $data{'trd_engine'};
	my $fst_params = delete $data{'fst_params'};
	my $snd_params = delete $data{'snd_params'};
	my $trd_params = delete $data{'trd_params'};

	my $total_spectra = 0;
	my $total_phospho_spectra = 0;

	my ($pepts_fst,  $pepts_snd,  $pepts_trd)  = (0, 0, 0);
	my ($fpepts_fst, $fpepts_snd, $fpepts_trd) = (0, 0, 0);
	my ($pepts_FS,  $pepts_FT,  $pepts_ST,  $pepts_FST)  = (0, 0, 0, 0);
	my ($fpepts_FS, $fpepts_FT, $fpepts_ST, $fpepts_FST) = (0, 0, 0, 0);

	my %pepts_nr;
	my %pepts_cons_nr;
	my %fpepts_nr;
	my %fpepts_nr_ascore;

	my %one_char_engine = ('Sequest'   => 'S',
						   'Phenyx'    => 'F',
						   'Omssa'     => 'O',
						   'OMSSA'     => 'O',   # deprecated
						   'Peaks'     => 'P',
						   'EasyProt'  => 'E',
						   'Discoverer'=> 'D',
						   'None'      => 'N'
						  );

	# unify phospho and DH and remove other PTMs
	foreach my $spectrum(keys %data){
		unless(defined $data{$spectrum}{'consensus_mod'}){
			next;
		}
		if(defined $data{$spectrum}{'ascore_data'}{'peptide'}){
			$data{$spectrum}{'consensus_mod'} = $data{$spectrum}{'ascore_data'}{'peptide'}
		}
		$data{$spectrum}{'consensus_mod'} =~ s/214|737|35//g;
		$data{$spectrum}{'consensus_mod'} =~ s/\(|\,|\)//g;
		$data{$spectrum}{'consensus_mod'} =~ s/S21|S23/s/g;
		$data{$spectrum}{'consensus_mod'} =~ s/T21|T23/t/g;
		$data{$spectrum}{'consensus_mod'} =~ s/Y21/y/g;
	}

	foreach my $spectrum(keys %data){
		# Only report consensus in global report !!
		unless(defined $data{$spectrum}{'consensus_mod'}){next;}
		$total_spectra++;
		my $phosphos = 0;
		my $spec_cons_mod = $data{$spectrum}{'consensus_mod'};
		my $dummy = $spec_cons_mod ;
		while($dummy =~ s/s|t|y/x/){$phosphos++;}

		if($phosphos){
			$total_phospho_spectra++;
			my ($a1, $a2, $a3) = (0, 0, 0);
			if(defined $data{$spectrum}{'ascore_data'}){
				if(defined $data{$spectrum}{'ascore_data'}{'ascore'}[0]){
					if($data{$spectrum}{'ascore_data'}{'ascore'}[0] > $limit_ascore){
						$a1 = 1;
					}
				}
				else{
					print $spectrum, "\n";
					$a1 = 6666;
				}
				if(defined $data{$spectrum}{'ascore_data'}{'ascore'}[1]){
					if($data{$spectrum}{'ascore_data'}{'ascore'}[1] > $limit_ascore){
						$a2 = 1;
					}
				}
				if(defined $data{$spectrum}{'ascore_data'}{'ascore'}[2]){
					if($data{$spectrum}{'ascore_data'}{'ascore'}[2] > $limit_ascore){
						$a3 = 1;
					}
				}
				if(defined $fpepts_nr_ascore{$spec_cons_mod}){
					$fpepts_nr_ascore{$spec_cons_mod}[0] += $a1;
					$fpepts_nr_ascore{$spec_cons_mod}[1] += $a2;
					$fpepts_nr_ascore{$spec_cons_mod}[2] += $a3;
				}
				else{
					@{$fpepts_nr_ascore{$spec_cons_mod}} = ($a1, $a2, $a3);
				}
			}
		}

		my ($fst, $snd, $trd) = (0, 0, 0);

		if((defined $data{$spectrum}{$fst_engine}{'unmodified_sequence'})and
		   ($data{$spectrum}{$fst_engine}{'unmodified_sequence'} eq $data{$spectrum}{'consensus'})){
			$fst = 1;
			$pepts_fst++;
		}
		if((defined $data{$spectrum}{$snd_engine}{'unmodified_sequence'})and
		   ($data{$spectrum}{$snd_engine}{'unmodified_sequence'} eq $data{$spectrum}{'consensus'})){
			$snd = 1;
			$pepts_snd++;
		}
		if ((defined $data{$spectrum}{$trd_engine}{'unmodified_sequence'})and
		   ($data{$spectrum}{$trd_engine}{'unmodified_sequence'} eq $data{$spectrum}{'consensus'})
		   ){
			$trd = 1;
			$pepts_trd++;
		}

		if(defined $pepts_cons_nr{$spec_cons_mod}){
			$pepts_cons_nr{$spec_cons_mod}++;
		}
		else{
			$pepts_cons_nr{$spec_cons_mod} = 1;
		}
		if($fst and $snd and $trd){
			$pepts_FS++;
			$pepts_FT++;
			$pepts_ST++;
			$pepts_FST++;
			if(defined $pepts_nr{$spec_cons_mod}){
				$pepts_nr{$spec_cons_mod}[0]++;
				$pepts_nr{$spec_cons_mod}[1]++;
				$pepts_nr{$spec_cons_mod}[2]++;
			}
			else{
				$pepts_nr{$spec_cons_mod} = [1, 1, 1];
			}
			if ($phosphos){
				$fpepts_fst++;
				$fpepts_snd++;
				$fpepts_trd++;
				$fpepts_FS++;
				$fpepts_FT++;
				$fpepts_ST++;
				$fpepts_FST++;
				if(defined $fpepts_nr{$spec_cons_mod}){
					$fpepts_nr{$spec_cons_mod}[0]++;
					$fpepts_nr{$spec_cons_mod}[1]++;
					$fpepts_nr{$spec_cons_mod}[2]++;
				}
				else{
					$fpepts_nr{$spec_cons_mod} = [1, 1, 1];
				}
			}
		}
		elsif($fst and $snd){
			$pepts_FS++;
			if(defined $pepts_nr{$spec_cons_mod}){
				$pepts_nr{$spec_cons_mod}[0]++;
				$pepts_nr{$spec_cons_mod}[1]++;
			}
			else{
				$pepts_nr{$spec_cons_mod} = [1, 1, 0];
			}
			if ($phosphos){
				$fpepts_fst++;
				$fpepts_snd++;
				$fpepts_FS++;
				if(defined $fpepts_nr{$spec_cons_mod}){
					$fpepts_nr{$spec_cons_mod}[0]++;
					$fpepts_nr{$spec_cons_mod}[1]++;
				}
				else{
					$fpepts_nr{$spec_cons_mod} = [1, 1, 0];
				}
			}
		}
		elsif($fst and $trd){
			$pepts_FT++;
			if(defined $pepts_nr{$spec_cons_mod}){
				$pepts_nr{$spec_cons_mod}[0]++;
				$pepts_nr{$spec_cons_mod}[2]++;
			}
			else{
				$pepts_nr{$spec_cons_mod} = [1, 0, 1];
			}
			if($phosphos){
				$fpepts_fst++;
				$fpepts_trd++;
				$fpepts_FT++;
				if(defined $fpepts_nr{$spec_cons_mod}){
					$fpepts_nr{$spec_cons_mod}[0]++;
					$fpepts_nr{$spec_cons_mod}[2]++;
				}
				else{
					$fpepts_nr{$spec_cons_mod} = [1, 0, 1];
				}
			}
		}
		elsif($snd and $trd){
			$pepts_ST++;
			if(defined $pepts_nr{$spec_cons_mod}){
				$pepts_nr{$spec_cons_mod}[1]++;
				$pepts_nr{$spec_cons_mod}[2]++;
			}
			else{
				$pepts_nr{$spec_cons_mod} = [0, 1, 1];
			}
			if ($phosphos){
				$fpepts_snd++;
				$fpepts_trd++;
				$fpepts_ST++;
				if(defined $fpepts_nr{$spec_cons_mod}){
					$fpepts_nr{$spec_cons_mod}[1]++;
					$fpepts_nr{$spec_cons_mod}[2]++;
				}
				else{
					$fpepts_nr{$spec_cons_mod} = [0, 1, 1];
				}
			}
		}
	}
	my ($F, $S, $T) =
	    ($one_char_engine{$fst_engine},
		 $one_char_engine{$snd_engine},
		 $one_char_engine{$trd_engine}
		);

	my $stats_file = $extended_report_file;
	$stats_file = (split /\./, $stats_file)[0];
	$stats_file = $stats_file . '_stats.xls';
	open my $STAT, '>', "$stats_file" or croak "unable to open: $!";
	print $STAT "Total consensus spectra:\t", $total_spectra, "\n";
	print $STAT "$fst_engine peptides:\t", $pepts_fst, "\n";
	print $STAT "$snd_engine peptides:\t", $pepts_snd, "\n";
	print $STAT "$trd_engine peptides:\t", $pepts_trd,"\n";
	print $STAT "$F+$S peptides:\t", $pepts_FS, "\n";
	print $STAT "$F+$T peptides:\t", $pepts_FT, "\n";
	print $STAT "$S+$T peptides:\t", $pepts_ST, "\n";
	print $STAT "$F+$S+$T peptides:\t", $pepts_FST, "\n\n";

	print $STAT "Total phospho consensus spectra:\t", $total_phospho_spectra, "\n";
	print $STAT "$fst_engine phospho-peptides:\t", $fpepts_fst, "\n";
	print $STAT "$snd_engine phospho-peptides:\t", $fpepts_snd, "\n";
	print $STAT "$trd_engine phospho-peptides:\t", $fpepts_trd, "\n";
	print $STAT "$F+$S phospho-peptides:\t", $fpepts_FS, "\n";
	print $STAT "$F+$T phospho-peptides:\t", $fpepts_FT, "\n";
	print $STAT "$S+$T phospho-peptides:\t", $fpepts_ST, "\n";
	print $STAT "$F+$S+$T phospho-peptides:\t", $fpepts_FST, "\n\n";

	my $peps_no_red = scalar keys %pepts_nr;
	my $fpeps_no_red = scalar keys %fpepts_nr;

	print $STAT "Non redundant peptides:\t", $peps_no_red, "\n";
	print $STAT "Non redundant phospho-peptides:\t", $fpeps_no_red, "\n\n\n";
	print $STAT "List of non-redundant peptides\t#Spectra\tPhospho\t$F\t$S\t" .
			   "$T\t#Ascore1>lim\t#Ascore2>lim\t#Ascore3>lim\n";

	foreach my $pep (sort(keys(%pepts_nr))) {
		print $STAT $pep, "\t";
		print $STAT $pepts_cons_nr{$pep}, "\t";
		if(defined $fpepts_nr{$pep}){
			print $STAT "1\t";
			print $STAT join("\t", @{$fpepts_nr{$pep}}), "\t";
			print $STAT join("\t", @{$fpepts_nr_ascore{$pep}}), "\t";
		}
		else{
			print $STAT "0\t";
			print $STAT join("\t", @{$pepts_nr{$pep}}), "\t";
			print $STAT ".\t" x 3, "\t";
		}
		print $STAT "\n";
	}
	close $STAT;
	return 1;
}


1;    #262


__END__


=encoding utf8

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
