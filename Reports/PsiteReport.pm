package Reports::PsiteReport;

use strict;
use warnings;
use Data::Dumper;

my $VERSION = '1.01';
our @EXPORT = qw(gen_psite_report);
use base qw(Exporter);

sub gen_psite_report{
	my ($data_ref, $report_file, $ascore_limit, $is_tmt_or_itraq) = @_;
	
	my %data = %{$data_ref};
	my %psites;

	############# Organize info on the basis of psites ################
	#############	    (Filling of hash psites)       ################
	foreach my $spectrum(keys %data){
		# report only on those with consensus in global report!
		unless(defined $data{$spectrum}{'consensus_mod'}){
			next;
		}
		my @ascores;

		# only phosphorylated
		my $phosphopept;
		if(defined $data{$spectrum}{'ascore_data'}{'peptide'}){
			$phosphopept = $data{$spectrum}{'ascore_data'}{'peptide'};
			if(defined $data{$spectrum}{'ascore_data'}{'ascore'}){
				@ascores = @{$data{$spectrum}{'ascore_data'}{'ascore'}};
			}
		}
		else{
			next;
		}

		# Group isoforms (@proteins array has only one element)
		my @proteins;	#=split /\|/,$data{$spectrum}{'consensus_proteins'};
		$proteins[0] = $data{$spectrum}{'consensus_proteins'};
		print Dumper \@proteins;

		# The protein grouping method used here is not optimal.
		# The grouping object is the subgroup of proteins assigned to each peptide.
		# The problems arises when two intersecting groups appear.
		# In this case there is a duplicated p-site for the peptides
		# belonging to the intersecting group.
		# In "Nov08" dataset there is at least one case:
		# O00499->
		#    O00499|O00499-2|O00499-3|O00499-4|O00499-5|O00499-6|O00499-7|O00499-8|O00499-9|O00499-10|O00499-11
		#    s_282
		#    G(214)NK(214)SPS(21)PPDGSPAATPEIR
		# O00499 ->
		#    O00499|O00499-2|O00499-6|O00499-11
		#    s_315
		#    V(214)NHEPEPAGGATPGATLPK(214)S(21)PSQLR
		#
		# The above example values correspond to:
		# First (group representative) protein->
		#    All proteins
		#    aa_#p-site in first protein
		#    corresponding peptide
		#
		# It must be taken into account that the psite position is given only
		# for the first protein. For other 'isoforms' the p-site should be 
		# located independently.


		my $aa_num;

		# Si agrupo segun proteinas representantes ("isoformas"),
		# este array solo tendra 1 elemento !!!
		# ['Q8WWI1(1507)|Q8WWI1-2(1507)|Q8WWI1-3(1173)|Q8WWI1-4(1507)']
		foreach(@proteins){
			$_ =~ s/\((\w+)\)//g;
			my ($prot, $aa_num) = ($_, $1);
			unless($aa_num){
				next;
			}
			my @phosphosites = @{get_phosphosites($phosphopept, $aa_num)};

			for(my $i=0; $i<$#phosphosites+1; $i++){
				if(defined $psites{$prot}{$phosphosites[$i]}[0]){
					my %dat;
					$dat{'spectrum'} = $spectrum;
					$dat{'phosphopeptide'} = $phosphopept;
					$dat{'valid'} = 0;
					if(@ascores){
						$dat{'ascore'} = $ascores[$i];
					}
					if ($is_tmt_or_itraq eq 'itraq'){
						if(defined $data{$spectrum}{'itraq_info'}){
							$dat{'itraq_info'} = $data{$spectrum}{'itraq_info'};
							my @quant = split /\|/, $data{$spectrum}{'itraq_info'};
							my $ratio;
							if(($quant[0] + $quant[1]) == 0){
								$ratio = 0;
							}
							else{
								$ratio = ($quant[2] + $quant[3]) /
								         ($quant[0] + $quant[1]);
							}
							$dat{'ratio'} = sprintf("%.3f", $ratio);
						}
					}
					elsif ($is_tmt_or_itraq eq 'tmt'){
						if(defined $data{$spectrum}{'tmt_info'}){
							$dat{'tmt_info'} = $data{$spectrum}{'tmt_info'};
							my @quant = split /\|/, $data{$spectrum}{'tmt_info'};
							my $ratio;
							if(($quant[0] + $quant[1]) == 0){
								$ratio = 0;
							}
							else{
								$ratio = ($quant[2] + $quant[3]) /
								         ($quant[0] + $quant[1]);
							}
							$dat{'ratio'} = sprintf("%.3f", $ratio);
						}
					}
					if(defined $data{$spectrum}{'ms2_spectral_info'}{'ms2_file'}){
						$dat{'ms2_info'} = $data{$spectrum}{'ms2_spectral_info'}{'ms2_file'};
					}
					$dat{'proteins_count'} = 1 + $#proteins;
					push @{$psites{$prot}{$phosphosites[$i]}[0]}, \%dat;
				}
				else{
					$psites{$prot}{$phosphosites[$i]}[0][0]{'spectrum'} =
					    $spectrum;
					$psites{$prot}{$phosphosites[$i]}[0][0]{'phosphopeptide'} =
					    $phosphopept;
					$psites{$prot}{$phosphosites[$i]}[0][0]{'valid'} = 0;
					if(@ascores){
						$psites{$prot}{$phosphosites[$i]}[0][0]{'ascore'} =
						    $ascores[$i];
					}
					if(defined $data{$spectrum}{'itraq_info'}){
						$psites{$prot}{$phosphosites[$i]}[0][0]{'itraq_info'} =
						    $data{$spectrum}{'itraq_info'};
						my @itraqs = split /\|/, $data{$spectrum}{'itraq_info'};
						my $ratio;
						if(($itraqs[0] + $itraqs[1]) == 0){
							$ratio = 0;
						}
						else{
							$ratio = ($itraqs[2] + $itraqs[3]) /
							         ($itraqs[0] + $itraqs[1]);
						}
						$psites{$prot}{$phosphosites[$i]}[0][0]{'ratio'} =
						    sprintf("%.3f", $ratio);
					}
					if(defined $data{$spectrum}{'ms2_spectral_info'}{'ms2_file'}){
						$psites{$prot}{$phosphosites[$i]}[0][0]{'ms2_info'} =
						    $data{$spectrum}{'ms2_spectral_info'}{'ms2_file'};
					}
					$psites{$prot}{$phosphosites[$i]}[0][0]{'proteins_count'} =
					    1 + $#proteins;
				}
			}
		}
	}
    #print Dumper \%psites;
	
############  Determine psite validity for global calculations  ###############
###########    (with ascore higher than the limit ascore and     ##############
##########   not being a redundant MS3 spectrum with valid MS2)  ##############

	foreach my $prot(keys %psites){
		LAB0:
		foreach my $psite(keys %{$psites{$prot}}){
			my (@i114, @i115, @i116, @i117);
			my @ratios;
			my $espectros_itraq_usados = 0;
			my @datos = @{$psites{$prot}{$psite}};
			# If Ascore is lower than $ascore_limit,
			# do not use itraq data for averaging
			LAB1:
			for(my $i=0; $i < $#{$datos[0]}+1; $i++){
				if(defined $datos[0][$i]{'ascore'}){
					if($datos[0][$i]{'ascore'} < $ascore_limit){
						next LAB1;
					}
				}
				## 1.- if MS3 and
				## 2.- the corresponding MS2 is listed and
				## 3.- MS2 ascore >= $ascore_limit:
				### no usamos este MS3
				if(defined $datos[0][$i]{'ms2_info'}){
					for (my $j=0; $j< $#{$datos[0]}+1; $j++){
						if($datos[0][$j]{'spectrum'} eq $datos[0][$i]{'ms2_info'}){  #punto2
							if($datos[0][$j]{'ascore'} >= $ascore_limit){            #punto 3
								next LAB1;
							}
						}
					}
				}
				#Si he llegado hasta aqui, etiqueto el espectro como valido
				$psites{$prot}{$psite}[0][$i]{'valid'} = 1;
			}
		}
	}

################################################################################
################################################################################
#print SAL Dumper %psites;         # dump %psites hash for debug

#####################     Generate psite report     ############################
##############         (calculate mean "on the fly")      ######################

	$report_file = $report_file . '.xls';
	open (SAL2, '>', "$report_file") or die $!;

	foreach my $prot(sort(keys %psites)){
		my $prot_represent = (split /\|/, $prot)[0];
		print SAL2 $prot_represent, "\t", $prot, "\n";
		foreach my $psite(keys %{$psites{$prot}}){
			print SAL2 "\t", $prot, "\t";
			print SAL2 $psite, "\t";
			my @datos = @{$psites{$prot}{$psite}};
			#informacion de promedios (solo con datos validos)
			my (@i114, @i115, @i116, @i117, @ratios_usados);
			my @itraqs;

			for(0..$#{$datos[0]}){
				if($datos[0][$_]{'valid'} eq 1){
					if(defined $datos[0][$_]{'itraq_info'}){
						push @ratios_usados, $datos[0][$_]{'ratio'};
					}
					else{
						#print "espectro ",$datos[0][$i]{'spectrum'}," sin iTRAQ info\n";;
					}
				}
			}

			my $promedio_true_ratios =
			    Math::NumberCruncher::Mean(\@ratios_usados);
			if(defined $promedio_true_ratios){
				$promedio_true_ratios =
				    sprintf("%.2f", $promedio_true_ratios);
			}
			else{
				$promedio_true_ratios = 'NC';
			}

			my $desvest_verdadero = standard_deviation(@ratios_usados);
			unless(defined $desvest_verdadero){
				$desvest_verdadero = 'NC';
			}
			my $CV_true_ratios = '';

			if($promedio_true_ratios eq 'NC'){
				$CV_true_ratios = 'NC';
			}
			elsif($promedio_true_ratios == 0){
				$CV_true_ratios = 'NC';
			}
			elsif($desvest_verdadero eq 'NC'){
				$CV_true_ratios = 'NC';
			}
			else{
				$CV_true_ratios =
				    100 * $desvest_verdadero / $promedio_true_ratios;
			}

			my $spectra_used = scalar @ratios_usados;

			if(defined $promedio_true_ratios){
				print SAL2 "\t\t\t\t", $promedio_true_ratios, "\t";
				print SAL2 $desvest_verdadero, "\t";
				print SAL2 $CV_true_ratios, "\t";
				print SAL2 "used specs:", $spectra_used;
			}
			else{
				print SAL2 "\t-\t-\t-\t-\t-\t-\t", "used specs:0";
			}

			print SAL2 "\n";

			for(my $i=0; $i < $#{$datos[0]}+1; $i++){
				print SAL2 "\t\t",$datos[0][$i]{'phosphopeptide'}, "\t";
				if(defined $datos[0][$i]{'itraq_info'}){
					my @itraqs = split /\|/, $datos[0][$i]{'itraq_info'};
					foreach(@itraqs){
						print SAL2 $_, "\t";
					}
					if($datos[0][$i]{'valid'} == 1){
						print SAL2 $datos[0][$i]{'ratio'}, "\t";
					}
					else{
						print SAL2 "[", $datos[0][$i]{'ratio'}, "]\t";
					}
				}
				else{
					print SAL2 "\t\t\t\t\t";
				}
				print SAL2 $datos[0][$i]{'spectrum'}, "\t";
				if(defined $datos[0][$i]{'ascore'} ){
					print SAL2 $datos[0][$i]{'ascore'}, "\t";
				}
				else{
					print SAL2 "\t";
				}
				print SAL2 "\n";
			}
			print SAL2 "\n";
		}
	}
	close SAL2;
}


sub get_phosphosites{
	# unifies phosphorylation and dehydration
	# returns an array of strings type "t_x",
	# where x is the position of the modification in the protein
	my ($pep, $aa_num) = @_;
	print $pep, "\n";
	my @phosphosites;
	$pep =~ s/(\(214\)|\(737\)|\(35\))//g;   #incluyo TMT!
	$pep =~ s/(214\,|\,214)//g;
	$pep =~ s/(737\,|\,737)//g;
	$pep =~ s/(S\(21\)|S\(23\))/s/g;
	$pep =~ s/(T\(21\)|S\(23\))/t/g;
	$pep =~ s/Y\(21\)/y/g;
	$pep =~ s/\(35\)//g;

	my @aas = split //, $pep;
	for(0..$#aas){
		if(($aas[$_] eq 's') or ($aas[$_] eq 't') or ($aas[$_] eq 'y')){
			push @phosphosites, $aas[$_] . '_' . ($_ + $aa_num);
		}
	}
	return \@phosphosites;
}


sub standard_deviation {
	my(@numbers) = @_;
	# prevent division by 0 error in case you get junk data
	my $divisor = scalar @numbers;
	$divisor = $divisor - 1;
	return undef unless($divisor);
	return undef unless(scalar(@numbers));

	# step 1, find the mean of the numbers
	my $total1 = 0;
	foreach my $num (@numbers) {
	$total1 += $num;
	}
	my $mean1 = $total1 / (scalar @numbers);

	# step 2, find the mean of the squares of the differences
	# between each number and the mean
	my $total2 = 0;
	foreach my $num (@numbers) {
	$total2 += ($mean1-$num)**2;
	}

	my $mean2 = $total2 / $divisor;

	# Step 3, standard deviation is the square root of the above mean
	my $std_dev = sqrt($mean2);
	return $std_dev;
}


return 1;          #344

__END__

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 LIMITATIONS

=head1 SEE ALSO

=cut
