#!/usr/bin/perl -w

use utf8;
use Wx 0.15 qw[:allclasses];
use strict;
use warnings;
use Data::Dumper;

package MyIntegratorFrame;

use Wx qw[:everything];
use Wx::Event qw(EVT_BUTTON);
use threads;
use Cwd qw(getcwd);
use File::Basename qw(dirname);
use Config::Tiny;
use Core::Reportation qw(read_json general_report psite_report);
use Core::Integration qw(integrate);
use Core::IntegratorFrame;
use Parsers::ParsersConstants qw(get_file_extensions);
use Data::Dumper;

use base qw(IntegratorFrame);

my @wins = q(xls_dir xls_file mgf_dir mgf_file json_dir json_file fasta_dir fasta_file);
my $config_file = "integrator.ini";

sub new {
	my $class = shift;
	my $self = $class->SUPER::new();
	$self->SetTitle("Integrator 3.5 The Next Generation and Beyond");

	foreach (@wins){$self->{$_} = undef}

	$self->{config_file} = $config_file;

	EVT_BUTTON($self, $self->{bt_fst_add}, \&get_fst_filenames);
	EVT_BUTTON($self, $self->{bt_fst_clr}, \&erase_fst_filenames);
	EVT_BUTTON($self, $self->{bt_snd_add}, \&get_snd_filenames);
	EVT_BUTTON($self, $self->{bt_snd_clr}, \&erase_snd_filenames);
	EVT_BUTTON($self, $self->{bt_trd_add}, \&get_trd_filenames);
	EVT_BUTTON($self, $self->{bt_trd_clr}, \&erase_trd_filenames);

	EVT_BUTTON($self, $self->{bt_mgf},     \&mgfsdir);
	EVT_BUTTON($self, $self->{bt_fasta},   \&get_fasta);
	EVT_BUTTON($self, $self->{bt_out},     \&set_json);
	EVT_BUTTON($self, $self->{bt_integrate}, \&integrator);

	EVT_BUTTON($self, $self->{bt_json},    \&get_json);
	EVT_BUTTON($self, $self->{bt_gen},     \&reporting);

	EVT_BUTTON($self, $self->{bt_edit},    \&edit_config);
	EVT_BUTTON($self, $self->{bt_refresh}, \&refresh_config_panel);

	$self->init_conf();

	bless($self, $class);     # reconsecrate
	return $self;
}


sub get_fst_filenames{
	my ($self, $event) = @_;

	if ($self->{fst_engine} eq 'None') {
		return;
	}

	my ($filetype, $dialog_key) = @{get_file_extensions($self->{fst_engine})};
	$self->{txt} = "Select a " . $self->{fst_engine} . " $filetype file";
	$self->get_files("lbx_fst", $dialog_key);
}

sub get_snd_filenames{
	my ($self, $event) = @_;

	if ($self->{snd_engine} eq 'None') {
		return;
	}

	my ($filetype, $dialog_key) = @{get_file_extensions($self->{snd_engine})};
	$self->{txt} = "Select a " . $self->{snd_engine} . " $filetype file";
	$self->get_files("lbx_snd", $dialog_key);
}

sub get_trd_filenames{
	my ($self, $event) = @_;

	if ($self->{trd_engine} eq 'None') {
		return;
	}

	my ($filetype, $dialog_key) = @{get_file_extensions($self->{trd_engine})};
	$self->{txt} = "Select a " . $self->{trd_engine} . " $filetype file";
	$self->get_files("lbx_trd", $dialog_key);
}

sub get_files{
	my ($self, $win, $dialog_key) = @_;
	$self->{xls_dir} = getcwd unless defined $self->{xls_dir};
	$self->{xls_file} = "" unless defined $self->{xls_file};
	my $dialog = Wx::FileDialog->new(
						$self, $self->{txt}, $self->{xls_dir}, $self->{xls_file},
						$dialog_key,
						wxFD_MULTIPLE,
						);

	if ($dialog->ShowModal == wxID_OK ) {
		my @paths = $dialog->GetPaths;
		$self->{xls_dir} = dirname($paths[0]);
		$self->{xls_file} = $paths[0];
		$self->{$win}->InsertItems(\@paths, 0);
	}
	$dialog->Destroy;
}

sub erase_fst_filenames{
	my ($self, $event) = @_;
	$self->{lbx_fst}->Clear;
}

sub erase_snd_filenames{
	my ($self, $event) = @_;
	$self->{lbx_snd}->Clear;
}

sub erase_trd_filenames{
	my ($self, $event) = @_;
	$self->{lbx_trd}->Clear;
}

sub get_fasta{
	my ($self, $event) = @_;
	$self->{fasta_dir} = getcwd unless defined $self->{fasta_dir};
	$self->{fasta_file} = "" unless defined $self->{fasta_file};
	my $dialog = Wx::FileDialog->new(
			    $self, "Select a Fasta database",
				$self->{fasta_dir}, $self->{fasta_file},
				"fasta (*.fasta, *.Fasta)|*.fasta|All files (*.*)|*.*", wxFD_OPEN
				);

	if ( $dialog->ShowModal == wxID_OK ) {
		my @paths = $dialog->GetPaths;
		$self->{fasta_dir} = dirname($paths[0]);
		$self->{fasta_file} = $paths[0];
		$self->{tc_fasta}->SetValue($paths[0]);
	}
	$dialog->Destroy;
}

sub set_json{
	my ($self, $event) = @_;
	$self->open_json("Select a Json out file", "tc_out", wxFD_SAVE);
}

sub get_json{
	my ($self, $event) = @_;
	$self->open_json("Select a Json database file", "tc_json", wxFD_OPEN);
}


sub open_json{
	my ($self, $txt, $win, $mode) = @_;
	$self->{json_dir} = getcwd unless defined $self->{json_dir};
	$self->{json_file} = "" unless defined $self->{json_file};
	my $dialog = Wx::FileDialog->new
					($self, $txt, $self->{json_dir}, $self->{json_file},
					"db (*.db, *.DB)|*.db|All files (*.*)|*.*", $mode);

	if ( $dialog->ShowModal == wxID_OK ) {
		my @paths = $dialog->GetPaths;
		$self->{json_dir} = dirname($paths[0]);
		$self->{json_file} = $paths[0];
		$self->{$win}->SetValue($paths[0]);
	}
	$dialog->Destroy;
}


sub mgfsdir{
	my ($self, $event ) = @_;

	unless (defined($self->{mgf_dir})){
		if ($self->{xls_dir}){
			$self->{mgf_dir} = $self->{xls_dir};
		}
		else{
			$self->{mgf_dir} = getcwd;
		}
	}

	my $dialog = Wx::DirDialog->new(
				 $self, 'Select the path where the mgf files are located',
				 $self->{mgf_dir}
				 );

	if ( $dialog->ShowModal == wxID_OK ) {
	  my $path = $dialog->GetPath;
	  $self->{mgf_dir} = $path;
	  $self->{tc_mgf}->Clear;
	  $self->{tc_mgf}->AppendText($path);
	}
	$dialog->Destroy;
}


sub integrator{
	my ($self, $event) = @_;

	my $mgfs_dir = $self->{tc_mgf}->GetValue();
	my @mgfs;
	opendir(my $DIR, $mgfs_dir) or die "can not open $!";
	while (defined(my $file = readdir($DIR))) {
		if ($file =~ /\.mgf$/){
			$file = $mgfs_dir . '\\' . $file;
			push @mgfs, $file;
		}
	}
	closedir($DIR);
	my $scan_method = $self->{rbx_smeth}->GetSelection;
	my @fst_files = $self->{lbx_fst}->GetStrings;
	my @snd_files = $self->{lbx_snd}->GetStrings;
	my @trd_files = $self->{lbx_trd}->GetStrings;
	my $fasta = $self->{tc_fasta}->GetValue;
	my $json  = $self->{tc_out}->GetValue;
	my $is_tmt_or_itraq = $self->{rbx_qmeth}->GetStringSelection;
	my $offset    = $self->{mass_offset};
	my $precision = $self->{mass_precision};
	my $experiment = $self->{tc_exp}->GetValue;

	my $num_engines = 0;
	my $last_engine = "";

	print "=== " . $self->{fst_engine} . " ========\n";
	foreach(@fst_files){
		print $_, "\n";
	}
	if($#fst_files > -1){
		$num_engines++;
		$last_engine = $self->{fst_engine};
	}
	print "=== " . $self->{snd_engine} . " ========\n";
	foreach(@snd_files){
		print $_, "\n";
	}
	if($#snd_files > -1){
		$num_engines++;
		$last_engine = $self->{snd_engine};
	}
	print "=== " . $self->{trd_engine} . " ========\n";
	foreach(@trd_files){
		print $_, "\n";
	}
	if($#trd_files > -1){
		$num_engines++;
		$last_engine = $self->{trd_engine};
	}
	print "\n=====================\n";
	my $one_engine = "";
	if($num_engines == 1){
		$one_engine = $last_engine;
	}
	print "Fasta DB: $fasta\n";
	print "Json output DB: $json\n";
	print "Experiment: $experiment\n";
	print "=====================\n";

	my $thrd = threads->create(\&integrate,
							   \@fst_files, \@snd_files, \@trd_files,
							   $self->{fst_engine}, $self->{snd_engine}, $self->{trd_engine},
							   $fasta, \@mgfs, $json, $one_engine,
							   $scan_method, $is_tmt_or_itraq,
							   $offset, $precision, $experiment);

	return 1;
}


sub reporting {
	my ($self, $event) = @_;
	my $json_file = $self->{tc_json}->GetValue;
	my $extended_report = $self->{cbx_exrep}->GetValue;
	my $psite_report = $self->{cbx_prep}->GetValue;
	my $limit_ascore = $self->{tc_asc}->GetValue;

	unless($limit_ascore){$limit_ascore = 0;}
	my $t0 = time();

	my $out_file = $json_file;
	$out_file =~ s/\.db$//;

	my $jsondata_ref = read_json($json_file);
	my %jsondata = %{$jsondata_ref};

	# Detect if quantification is iTRAQ or TMT.
	my $is_tmt_or_itraq = "none";
	foreach my $spectrum(keys %jsondata){
		if ($spectrum =~ /[fst|snd|trd]_[engine|params]/) {
				next;
		}

		#print $spectrum, "\n";
		#print Dumper($jsondata{$spectrum}, "\n");
		if($jsondata{$spectrum}{'consensus_mod'} =~ /214/){
			$is_tmt_or_itraq = "itraq";
			last;
		}
		elsif($jsondata{$spectrum}{'consensus_mod'} =~ /737/){
			$is_tmt_or_itraq = "tmt";
			last;
		}
	}

	# Get engines and corresponding params.
	# Reporting works taking data from a json database.
	# Engines had to be taken from that json file and not from
	# integrator defaults because user not neccesarily knows how the
	# db was created (like for itraq/tmt)
	if($extended_report){
		general_report($jsondata_ref, $out_file,
					   $limit_ascore, $is_tmt_or_itraq
		);
	}
	if($psite_report){
		psite_report($jsondata_ref, $out_file,
					 $limit_ascore, $is_tmt_or_itraq
		);
	}

	my $tt = time() - $t0;

	print $tt, " seconds\n";
	print "======================\n";
	print " Reporting complete\n";
	print "======================\n";
}


sub init_conf{
	my $self = shift;
	$self->{lb_thisfile_val}->SetLabel($self->{config_file});

	# create a config
	my $Config = Config::Tiny->new;
	# Open the config
	$Config = Config::Tiny->read( $self->{config_file} );

	# files and directories
	$self->{xls_dir}  = $Config->{_}->{xlsdir};
	$self->{mgf_dir}  = $Config->{_}->{mgfdir};
	$self->{json_dir} = $Config->{_}->{jsondir};
	$self->{fasta_file} = $Config->{_}->{fastafile};

	$self->{fasta_dir} = dirname($self->{fasta_file});

	# quantitation
	my ($itraq_mass, $tmt_mass) = (114.1112, 126.1283);
	my $offset = $Config->{_}->{massoffset};
	my $itraq_off = $itraq_mass + $offset;
	my $tmt_off = $tmt_mass + $offset;
	my $help_mass = "iTRAQ-$itraq_mass ---> $itraq_off     " .
	                "TMT-$tmt_mass ---> $tmt_off";

	$self->{mass_offset} = $offset;
	$self->{mass_precision} = $Config->{_}->{massprecision};

	my $precision = chr(177) . " $self->{mass_precision}  uma";

	# search Engines
	$self->{fst_engine} = $Config->{_}->{fst_engine};
	$self->{snd_engine} = $Config->{_}->{snd_engine};
	$self->{trd_engine} = $Config->{_}->{trd_engine};

	# fill integration panel
	$self->{tc_mgf}->SetValue($self->{mgf_dir});
	$self->{tc_fasta}->SetValue($self->{fasta_file});

	$self->{lb_fst}->SetLabel("     " . $self->{fst_engine});
	$self->{lb_snd}->SetLabel("     " . $self->{snd_engine});
	$self->{lb_trd}->SetLabel("     " . $self->{trd_engine});

	# fill config panel
	$self->{lb_xlsdir_val}->SetLabel($self->{xls_dir});
	$self->{lb_mgfdir_val}->SetLabel($self->{mgf_dir});
	$self->{lb_jsondir_val}->SetLabel($self->{json_dir});
	$self->{lb_fasta_val}->SetLabel($self->{fasta_file});
	$self->{lb_fst_val}->SetLabel($self->{fst_engine});
	$self->{lb_snd_val}->SetLabel($self->{snd_engine});
	$self->{lb_trd_val}->SetLabel($self->{trd_engine});
	$self->{lb_offset_val}->SetLabel($self->{mass_offset} . ' uma');
	$self->{lb_mass_val}->SetLabel($help_mass);
	$self->{lb_precision_val}->SetLabel($precision);
}

sub edit_config{
	my $self = shift;
	system("start $self->{config_file}");
}

sub refresh_config_panel{
	my $self = shift;
	$self->init_conf();
}




package main;
use Wx::App;

undef &Wx::App::OnInit;
*Wx::App::OnInit = sub{1};
my $app = Wx::App->new();
Wx::InitAllImageHandlers();

my $frame = MyIntegratorFrame->new();

$app->SetTopWindow($frame);
$frame->Show(1);
$app->MainLoop();

1;                         #297
